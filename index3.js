var axios = require("axios").default;

var options = {
  method: 'POST',
  url: 'https://restb-ai-watermark-detection.p.rapidapi.com/wmdetect',
  headers: {
    'content-type': 'application/x-www-form-urlencoded',
    'x-rapidapi-host': 'restb-ai-watermark-detection.p.rapidapi.com',
    'x-rapidapi-key': '3cba680dabmsha162b922fb20757p18d43ejsn23ca6476268d'
  },
  data: {
    image_url: 'https://obs.mtlab.meitu.com/mtopen/VJ5hQlfjou7Q9qRokCyrP9KkoZjTbxBG/MTYzNjU2MDAwMA==/001f8a33-2aaf-4698-acba-a1160a10ac02.jpg'
  }
};

axios.request(options).then(function (response) {
	console.log(response.data);
}).catch(function (error) {
	console.error(error);
});