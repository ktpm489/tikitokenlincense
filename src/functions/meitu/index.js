const rp = require("request-promise");
const { cloudData, meituData } = require("../../config/configData");
const cloudinary = require("cloudinary");
const { processData, parseFaceResult } = require("../../utils/parseSkin");
const { processData: processDataV2, parseFaceResult: parseFaceResultV2 } = require("../../utils/parseSkinV2");
const { processData: processDataV3, parseFaceResult: parseFaceResultV3} = require("../../utils/parseSkinV3");
const { processData: processDataV4, parseFaceResult: parseFaceResultV4} = require("../../utils/parseSkinV4");
const fetch = require("node-fetch");
const cheerio = require("cheerio");
const axios = require("axios");
const randomUseragent = require("random-useragent");
const https = require("https");
const NodeCache = require("node-cache");
const Keydemo = require("../../models/keyMeitu");
const KeySkin = require("../../models/keySkin");
//cache 5 minutes
const myCache = new NodeCache({ stdTTL: 20 * 60, checkperiod: 120 });
// At request level
const agent = new https.Agent({
  rejectUnauthorized: false,
});
async function loadConfigAI() {
  try {
    let response = await axios.get("https://jpst.it/2sGv3", {
      httpsAgent: agent,
    });
    const $ = cheerio.load(response.data);
    let data = $("#articleContent").children("p").text();
    if (data !== "") {
      myCache.set("myKey", data, 5 * 60);
      return data;
    } else {
      data = $("#articleContent")
        .children("div")
        .children("div")
        .children("span")
        .text();
      if (data !== "") {
        myCache.set("myKey", data, 5 * 60);
        return data;
      } else {
        let data = $("#articleContent").children("p").children("span").text();
        myCache.set("myKey", data, 5 * 60);
        return data;
      }
    }
    // console.log("data", data);
    // let response = await axios.get("https://csgadmin.com/api/key");
    // console.log("response", response);
    // let data = response.data;
    // console.log("data", data);
    // let data = data[0].key;
    // return data[0].key;
    // return "";
  } catch (e) {
    console.log("e", e);
    return "";
  }
}

async function loadConfigAIv2() {
  try {

    const response = await Keydemo.find()
    if (response !== undefined){
      // console.log('responseLoadData', response)
      myCache.set("myKey", response[0].key, 5 * 60);
      return response[0].key;
    } else {
      let response = await axios.get("https://jpst.it/2sGv3", {
        httpsAgent: agent,
      });
      const $ = cheerio.load(response.data);
      let data = $("#articleContent").children("p").text();
      if (data !== "") {
        myCache.set("myKey", data, 5 * 60);
        return data;
      } else {
        data = $("#articleContent")
          .children("div")
          .children("div")
          .children("span")
          .text();
        if (data !== "") {
          myCache.set("myKey", data, 5 * 60);
          return data;
        } else {
          let data = $("#articleContent").children("p").children("span").text();
          myCache.set("myKey", data, 5 * 60);
          return data;
        }
      }
    }
   
  } catch (e) {
    console.log("e", e);
    let response = await axios.get("https://jpst.it/2sGv3", {
      httpsAgent: agent,
    });
    const $ = cheerio.load(response.data);
    let data = $("#articleContent").children("p").text();
    if (data !== "") {
      myCache.set("myKey", data, 5 * 60);
      return data;
    } else {
      data = $("#articleContent")
        .children("div")
        .children("div")
        .children("span")
        .text();
      if (data !== "") {
        myCache.set("myKey", data, 5 * 60);
        return data;
      } else {
        let data = $("#articleContent").children("p").children("span").text();
        myCache.set("myKey", data, 5 * 60);
        return data;
      }
    }
  }
}

async function callDataV2(data) {
  try {
    let keyApi = "";
    let value = myCache.get("myKey");
    // console.log("vo ko11  ", value);
    if (value == undefined) {
      // handle miss!
      keyApi = await loadConfigAIv2();
    } else {
      // console.log("vo ko", value);
      keyApi = value;
    }

    // let keyApi =
    // "A7VyjbkRIv9R9Ap/l0Px62nub0suaz1RYXA5aGNEeXpUWVc4TVUwUDI0R29CV2hyODF1Z0tfaCZ0PTE2MjM4NDAxMTEmcj01NTc3MjE2NDA5ODM5MjAxNzQyJmY9";
    // console.log("keyApi", keyApi);
    let random = randomUseragent.getRandom(function (ua) {
      return ua.browserName === "Firefox";
    });
    let res = await fetch("https://openapi.mtlab.meitu.com/v1/skinanlysis", {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
        "Accept-Encoding": "gzip, deflate, br",
        authority: "openapi.mtlab.meitu.com",
        authorization: keyApi,
        authorizationtype: 1,
        "user-agent": random,
        // "user-agent":
        //   "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.192 Safari/537.36",
      },
    });
    let json = await res.json();
    return json;
    // console.log("json", json);
    // console.log(json.media_info_list[0].media_extra.faces[0].face_attributes);
  } catch (e) {
    await loadConfigAI();
    // await callData(data);
    // console.log("e", e);
  }
}


async function callData(data) {
  try {
    let keyApi = "";
    let value = myCache.get("myKey");
    // console.log("vo ko11  ", value);
    if (value == undefined) {
      // handle miss!
      keyApi = await loadConfigAI();
    } else {
      // console.log("vo ko", value);
      keyApi = value;
    }

    // let keyApi =
    // "A7VyjbkRIv9R9Ap/l0Px62nub0suaz1RYXA5aGNEeXpUWVc4TVUwUDI0R29CV2hyODF1Z0tfaCZ0PTE2MjM4NDAxMTEmcj01NTc3MjE2NDA5ODM5MjAxNzQyJmY9";
    // console.log("keyApi", keyApi);
    let random = randomUseragent.getRandom(function (ua) {
      return ua.browserName === "Firefox";
    });
    let res = await fetch("https://openapi.mtlab.meitu.com/v1/skinanlysis", {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
        "Accept-Encoding": "gzip, deflate, br",
        authority: "openapi.mtlab.meitu.com",
        authorization: keyApi,
        authorizationtype: 1,
        "user-agent": random,
        // "user-agent":
        //   "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.192 Safari/537.36",
      },
    });
    let json = await res.json();
    return json;
    // console.log("json", json);
    // console.log(json.media_info_list[0].media_extra.faces[0].face_attributes);
  } catch (e) {
    await loadConfigAI();
    // await callData(data);
    // console.log("e", e);
  }
}
cloudinary.config({
  cloud_name: cloudData.cloud_name,
  api_key: cloudData.api_key,
  api_secret: cloudData.api_secret,
});

const API = {
  URL: meituData.URL,
  API_KEY: meituData.API_KEY,
  API_SECRET: meituData.API_SECRET,
};

async function doRequest(formBody) {
  const options = {
    method: "POST",
    url: API.URL,
    qs: {
      api_key: API.API_KEY,
      api_secret: API.API_SECRET,
    },
    json: true,
    body: formBody,
    resolveWithFullResponse: true,
  };
  try {
    return await rp(options);
  } catch (e) {
    console.error(e);
    return e.response;
  }
}

function createFormToDetect(dataImage) {
  return {
    parameter: {
      nFront: 1,
    },
    media_info_list: [
      {
        media_data: dataImage,
        media_profiles: {},
      },
    ],
  };
}

async function cropImage(base64Image, faces) {
  let image_cropped_list = [];
  for (const face of faces) {
    let imageCropped = "";
    let cropInfo = {
      x: Math.ceil(face.face_rectangle.left),
      y: Math.ceil(face.face_rectangle.top),
      width: Math.ceil(face.face_rectangle.width),
      height: Math.ceil(face.face_rectangle.height),
      crop: "crop",
    };
    await cloudinary.v2.uploader.upload(
      base64Image,
      {
        transformation: cropInfo,
        public_id: "uploads/cropped_images/" + Date.now(),
      },
      function (error, result) {
        // console.log(result, error);
        imageCropped = result.url;
      }
    );
    image_cropped_list.push(imageCropped);
  }
  return image_cropped_list;
}

async function saveOriginalImage(base64Image) {
  let imageInfo = {};
  await cloudinary.v2.uploader.upload(
    base64Image,
    { public_id: "uploads/images/" + Date.now() },
    function (error, result) {
      // console.log(result, error);
      if (result) {
        imageInfo.width = result.width;
        imageInfo.height = result.height;
        imageInfo.url = result.url;
      } else {
        imageInfo.width = 0;
        imageInfo.height = 0;
        imageInfo.url = "";
      }
    }
  );
  return imageInfo;
}

exports.detectImage = async function detectImage(base64Image, typeSdk) {
  const dataForm = createFormToDetect(base64Image);
  let detectedFaces = {};
  const strBase64 = "data:image/jpeg;base64," + base64Image;
  const resultArray1 = await Promise.all([
    callDataV2(dataForm),
    saveOriginalImage(strBase64),
  ]);
  let data = resultArray1[0];
  detectedFaces.image_info = resultArray1[1];
  if (data) {
    const faces = data.media_info_list[0].media_extra.faces;
    if (faces.length > 1) {
      return {
        ErrorMsg: "Multiple faces detected. Please take another image.",
      };
    }

    //Save original image
    // detectedFaces.image_info = await saveOriginalImage(strBase64);
    // console.log("faces[0].face_attributes", faces[0].face_attributes);
    const resultArray = await Promise.all([
      processData(faces[0].face_attributes, typeSdk),
    ]);
    detectedFaces = { ...detectedFaces, ...resultArray[0] };
    return detectedFaces;
  } else {
    if (response.statusCode === 400) {
      if (response.body.ErrorCode === 20003) {
        return {
          ErrorMsg: "Face Missing. Please take another image.",
        };
      }
    }
    return {
      ErrorMsg:
        "Sorry, there is a transmission error. Please check your Internet connection and try again later.",
    };
  }
};

// add store face
exports.detectImageV2 = async function detectImage(
  base64Image,
  typeSdk = "123456789"
) {
  const dataForm = createFormToDetect(base64Image);
  let detectedFaces = {};
  const strBase64 = "data:image/jpeg;base64," + base64Image;
  const resultArray1 = await Promise.all([
    callDataV2(dataForm),
    saveOriginalImage(strBase64),
  ]);
  let data = resultArray1[0];
  detectedFaces.image_info = resultArray1[1];
  if (data) {
    const faces = data.media_info_list[0].media_extra.faces;
    if (faces.length > 1) {
      return {
        ErrorMsg: "Multiple faces detected. Please take another image.",
      };
    }

    //Save original image
    // detectedFaces.image_info = await saveOriginalImage(strBase64);
    // console.log("faces[0].face_attributes", faces[0].face_attributes);
    const resultArray = await Promise.all([
      processData(faces[0].face_attributes, typeSdk),
      parseFaceResult(faces[0]),
    ]);
    detectedFaces = [{ ...detectedFaces, ...resultArray[0] }, resultArray[1]];
    return detectedFaces;
  } else {
    if (response.statusCode === 400) {
      if (response.body.ErrorCode === 20003) {
        return {
          ErrorMsg: "Face Missing. Please take another image.",
        };
      }
    }
    return {
      ErrorMsg:
        "Sorry, there is a transmission error. Please check your Internet connection and try again later.",
    };
  }
};


exports.detectImageV3 = async function detectImage(
  base64Image,
  typeSdk = "123456789"
) {
  const dataForm = createFormToDetect(base64Image);
  let detectedFaces = {};
  const strBase64 = "data:image/jpeg;base64," + base64Image;
  const resultArray1 = await Promise.all([
    callDataV2(dataForm),
    saveOriginalImage(strBase64),
  ]);
  let data = resultArray1[0];
  detectedFaces.image_info = resultArray1[1];
  if (data) {
    const faces = data.media_info_list[0].media_extra.faces;
    if (faces.length > 1) {
      return {
        ErrorMsg: "Multiple faces detected. Please take another image.",
      };
    }

    //Save original image
    // detectedFaces.image_info = await saveOriginalImage(strBase64);
    // console.log("faces[0].face_attributes", faces[0].face_attributes);
    const resultArray = await Promise.all([
      processDataV2(faces[0].face_attributes, typeSdk),
      parseFaceResultV2(faces[0]),
    ]);
    detectedFaces = [{ ...detectedFaces, ...resultArray[0] }, resultArray[1]];
    return detectedFaces;
  } else {
    if (response.statusCode === 400) {
      if (response.body.ErrorCode === 20003) {
        return {
          ErrorMsg: "Face Missing. Please take another image.",
        };
      }
    }
    return {
      ErrorMsg:
        "Sorry, there is a transmission error. Please check your Internet connection and try again later.",
    };
  }
};


exports.detectImageOutsource = async function detectImage(
  base64Image,
  typeSdk = "123456789"
) {
  const dataForm = createFormToDetect(base64Image);
  let detectedFaces = {};
  const strBase64 = "data:image/jpeg;base64," + base64Image;
  const resultArray1 = await Promise.all([
    callDataV2(dataForm),
    saveOriginalImage(strBase64),
  ]);
  let data = resultArray1[0];
  detectedFaces.image_info = resultArray1[1];
  if (data) {
    const faces = data.media_info_list[0].media_extra.faces;
    if (faces.length > 1) {
      return {
        ErrorMsg: "Multiple faces detected. Please take another image.",
      };
    }

    //Save original image
    // detectedFaces.image_info = await saveOriginalImage(strBase64);
    // console.log("faces[0].face_attributes", faces[0].face_attributes);
    const resultArray = await Promise.all([
      processDataV2(faces[0].face_attributes, typeSdk),
      parseFaceResultV2(faces[0]),
    ]);
    detectedFaces = [{ ...detectedFaces, ...resultArray[0] }, resultArray[1]];
    return detectedFaces;
  } else {
    if (response.statusCode === 400) {
      if (response.body.ErrorCode === 20003) {
        return {
          ErrorMsg: "Face Missing. Please take another image.",
        };
      }
    }
    return {
      ErrorMsg:
        "Sorry, there is a transmission error. Please check your Internet connection and try again later.",
    };
  }
};



exports.detectImageOutsourceV2 = async function detectImage(
  base64Image,
  typeSdk = "123456789"
) {
  const dataForm = createFormToDetect(base64Image);
  let detectedFaces = {};
  const strBase64 = "data:image/jpeg;base64," + base64Image;
  const resultArray1 = await Promise.all([
    callDataV2(dataForm),
    saveOriginalImage(strBase64),
  ]);
  let data = resultArray1[0];
  detectedFaces.image_info = resultArray1[1];
  if (data) {
    const faces = data.media_info_list[0].media_extra.faces;
    if (faces.length > 1) {
      return {
        ErrorMsg: "Multiple faces detected. Please take another image.",
      };
    }

    let response = await KeySkin.find()
    let objConfig = response ? (response[0].key) : null
    // console.log(typeof objConfig, objConfig, objConfig.BlackHead_Area === 'true')
    //Save original image
    // detectedFaces.image_info = await saveOriginalImage(strBase64);
    // console.log("faces[0].face_attributes", faces[0].face_attributes);
    const resultArray = await Promise.all([
      processDataV3(faces[0].face_attributes, typeSdk, objConfig),
      parseFaceResultV3(faces[0], objConfig),
    ]);
    detectedFaces = [{ ...detectedFaces, ...resultArray[0] }, resultArray[1], { rectangle: data.media_info_list[0].media_extra.faces[0].face_rectangle}];
    return detectedFaces;
  } else {
    if (response.statusCode === 400) {
      if (response.body.ErrorCode === 20003) {
        return {
          ErrorMsg: "Face Missing. Please take another image.",
        };
      }
    }
    return {
      ErrorMsg:
        "Sorry, there is a transmission error. Please check your Internet connection and try again later.",
    };
  }
};


exports.detectImageOutsourcePenisila = async function detectImage(
  base64Image,
  typeSdk = "123456789"
) {
  const dataForm = createFormToDetect(base64Image);
  let detectedFaces = {};
  const strBase64 = "data:image/jpeg;base64," + base64Image;
  const resultArray1 = await Promise.all([
    callDataV2(dataForm),
    saveOriginalImage(strBase64),
  ]);
  let data = resultArray1[0];
  detectedFaces.image_info = resultArray1[1];
  if (data) {
    const faces = data.media_info_list[0].media_extra.faces;
    if (faces.length > 1) {
      return {
        ErrorMsg: "Multiple faces detected. Please take another image.",
      };
    }

    let response = await KeySkin.find()
    let objConfig = response ? (response[0].key) : null
    // console.log(typeof objConfig, objConfig, objConfig.BlackHead_Area === 'true')
    //Save original image
    // detectedFaces.image_info = await saveOriginalImage(strBase64);
    // console.log("faces[0].face_attributes", faces[0].face_attributes);
    const resultArray = await Promise.all([
      processDataV4(faces[0].face_attributes, typeSdk, objConfig),
      parseFaceResultV4(faces[0], objConfig),
    ]);
    detectedFaces = [{ ...detectedFaces, ...resultArray[0] }, resultArray[1]];
    return detectedFaces;
  } else {
    if (response.statusCode === 400) {
      if (response.body.ErrorCode === 20003) {
        return {
          ErrorMsg: "Face Missing. Please take another image.",
        };
      }
    }
    return {
      ErrorMsg:
        "Sorry, there is a transmission error. Please check your Internet connection and try again later.",
    };
  }
};