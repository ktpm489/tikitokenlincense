// config.js
const dotenv = require('dotenv');
dotenv.config();

let cloudData  = {
    cloud_name: process.env.CLOUD_NAME, 
    api_key: process.env.CLOUD_KEY,  
    api_secret: process.env.CLOUD_SECRET
}

let meituData = {
    URL: process.env.URL_MEITU,
    API_KEY: process.env.MEITU_KEY,
    API_SECRET: process.env.MEITU_SECRET
}



module.exports = {
    meituData: meituData,
    cloudData: cloudData
};