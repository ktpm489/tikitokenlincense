const express = require("express");
const { check } = require("express-validator");
const multer = require("multer");
const { trackingAuth, trackingOutSourceAuth } = require("../middlewares/trackingAuthenticate");
const UserSkin = require("../controllers/userskin");
const validate = require("../middlewares/validate");
const rateLimit = require("express-rate-limit");
const router = express.Router();
const apiLimiter = rateLimit({
  windowMs: 1 * 60 * 1000, // 1 minutes
  max: 20,
  message: "Too many call from this IP, please try again after an 2 minutes",
});

//INDEX
router.get("/", UserSkin.index);
//STORE
router.post(
  "/",
  [
    // check('image_base64').not().isEmpty().withMessage('Enter a valid base64 image')
  ],
  validate,
  trackingAuth,
  apiLimiter,
  UserSkin.storeV2
);

router.post(
  "/v2",
  [
    // check('image_base64').not().isEmpty().withMessage('Enter a valid base64 image')
  ],
  validate,
  trackingAuth,
  apiLimiter,
  UserSkin.storeV2
);

router.post(
  "/v3",
  [
    // check('image_base64').not().isEmpty().withMessage('Enter a valid base64 image')
  ],
  validate,
  trackingAuth,
  apiLimiter,
  UserSkin.storeV3
);



//SHOW
router.get("/:id", UserSkin.show);
router.get("/user/:email", UserSkin.showByUserEmail);
router.get("/byuser/:email", UserSkin.showByUserEmailAndQuery);
router.get("/byid/:id", UserSkin.showById);

//UPDATE
// router.put('/:id', upload, User.update);

//DELETE
// router.delete('/:id', User.destroy);


// outsource version
router.post(
  "/trial",
  validate,
  trackingOutSourceAuth,
  apiLimiter,
  UserSkin.storeOutSource
);

router.post(
  "/pensillia",
  validate,
  trackingOutSourceAuth,
  apiLimiter,
  UserSkin.storeOutSourcePenisila
);
router.post(
  "/trialV2",
  validate,
  trackingOutSourceAuth,
  apiLimiter,
  UserSkin.storeOutSourceV2
);

router.post(
  "/menard",
  validate,
  trackingOutSourceAuth,
  apiLimiter,
  UserSkin.storeOutSourceV2
);





module.exports = router;
