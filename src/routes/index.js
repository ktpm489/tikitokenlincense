const auth = require("./auth");
const user = require("./user");
const tracking = require("./tracking");
const userskin = require("./userskin");
const faceskin = require("./faceskin");
const key = require("./key");
const keyMeitu = require("./keyMeitu");
const keySkin = require("./keySkin");

const authenticate = require("../middlewares/authenticate");
const { trackingAuth } = require("../middlewares/trackingAuthenticate");

module.exports = (app) => {
  app.get("/", (req, res) => {
    res.status(200).send({
      message:
        "Welcome to the AUTHENTICATION API. Register or Login to test Authentication.",
    });
  });
  app.use("/api/auth", auth);
  app.use("/api/user", authenticate, user);

  // edit tracking
  app.use("/api/tracking", authenticate, tracking);
  app.use("/api/userskin", userskin);
  app.use("/api/faceskin", faceskin);
  app.use("/api/key", key);
  app.use("/api/keyMeitu", keyMeitu);
  app.use("/api/keySkin", keySkin);
  // example tracking call api count
  // app.use('/api/user', authenticate, trackingAuth, user);
};
