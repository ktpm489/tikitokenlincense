const express = require("express");
const keydemo = require("../controllers/key");
const router = express.Router();

// Create a new Tutorial
router.post("/", keydemo.create);

// Retrieve all keydemo
router.get("/", keydemo.findAll);

// Retrieve all published keydemo
router.get("/published", keydemo.findAllPublished);

// Retrieve a single Tutorial with id
router.get("/:id", keydemo.findOne);

// Update a Tutorial with id
router.put("/:id", keydemo.update);

// Delete a Tutorial with id
router.delete("/:id", keydemo.delete);

// Create a new Tutorial
router.delete("/", keydemo.deleteAll);
// get tutorial by user
router.get("/byUser/:id", keydemo.findAllByUser);

module.exports = router;
