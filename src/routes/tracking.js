const express = require('express');
const {check} = require('express-validator');
const multer = require('multer');

const Tracking = require('../controllers/tracking');
const validate = require('../middlewares/validate');

const router = express.Router();

//INDEX
router.get('/', Tracking.index);

//STORE
router.post('/', [
    check('userId').not().isEmpty().withMessage('User Id is not valid'),
    check('type').not().isEmpty().withMessage('Attribute is not valid'),
    check('typeSdk').not().isEmpty().withMessage('Attribute is not valid'),
], validate, Tracking.store);

//SHOW
router.get('/:id', Tracking.show);
router.get('/userId/:id', Tracking.showByUserId);

//UPDATE
router.put('/:id', [
    check('userId').not().isEmpty().withMessage('User Id is not valid'),
    check('type').not().isEmpty().withMessage('Attribute is not valid'),
    check('typeSdk').not().isEmpty().withMessage('Attribute is not valid'),
], validate, Tracking.update);

//DELETE
router.delete('/:id', Tracking.destroy);

module.exports = router;


