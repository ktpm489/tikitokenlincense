const express = require("express");
const keydemo = require("../controllers/keySkin");
const router = express.Router();

router.post("/", keydemo.create);
router.get("/", keydemo.findData);

module.exports = router;
