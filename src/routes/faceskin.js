const express = require("express");
const { check } = require("express-validator");
const multer = require("multer");
const FaceUser = require("../controllers/faceuser");
const rateLimit = require("express-rate-limit");
const router = express.Router();
const apiLimiter = rateLimit({
  windowMs: 1 * 60 * 1000, // 1 minutes
  max: 20,
  message: "Too many call from this IP, please try again after an 2 minutes",
});

router.get("/users/:userid/faces", FaceUser.getUserFaces);
router.get("/users/:userid/faces/last", FaceUser.getLastUserFace);
router.get(
  "/users/:userid/faces/lastSixMonth",
  FaceUser.getLastUserFaceSixMonth
);
router.get("/faces/:id", FaceUser.getUserFace);
router.delete("/faces/:id", FaceUser.deleteUserFace);

module.exports = router;
