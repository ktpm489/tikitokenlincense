const Tracking = require("../models/tracking");
const { uploader, sendEmail } = require("../utils/index");
var ObjectId = require("mongoose").Types.ObjectId;
const {
  generateToken,
  configLimitCall,
  configDayCall,
} = require("../utils/index");
// @route GET tracking
// @desc Returns all tracking
// @access Public
exports.index = async function (req, res) {
  res.status(200).json({ data: "No Data" });
};

// @route POST api/tracking
// @desc Add a new tracking
// @access Public
exports.store = async (req, res) => {
  try {
    const { userId, typeSdk, type } = req.body;
    // Make sure post in role admin
    if (req.user.role !== "admin") {
      return res
        .status(401)
        .json({ message: `You don't have permision create it.` });
    }
    // Make sure this doesn't already exist
    const tracking = await Tracking.findOne({ userId: new ObjectId(userId) });
    if (tracking)
      return res.status(401).json({
        message:
          "The user you have entered is already associated with another key.",
      });
    let typeInput = type ? type : "trial";
    let typeSdkInput = typeSdk ? typeSdk : "1";
    let generateTokenData = generateToken(userId + "_" + typeSdkInput);
    let payload = {
      userId: userId,
      typeSdk: typeSdkInput,
      token: generateTokenData,
      limit: configLimitCall(typeInput),
      dateExpires: Date.now() + configDayCall(typeInput) * 1000 * 60 * 60 * 24,
    };
    const trackingSave = new Tracking(payload);
    const data = await trackingSave.save();
    // console.log('data',data)
    res.status(200).json({ message: "Create new token is sucess" });
  } catch (error) {
    res.status(500).json({ success: false, message: error.message });
  }
};

// @route GET api/tracking/{id}
// @desc Returns a specific tracking data
// @access Public
exports.show = async function (req, res) {
  try {
    const id = req.params.id;
    // Make sure post in role admin or it is owner of tracking key
    // console.log(req.user, req.user.tokenId, id);
    if (req.user.role === "admin" || req.user.tokenId === id) {
      const tracking = await Tracking.findById(id);
      if (!tracking)
        return res.status(401).json({ message: "Item does not exist" });
      let data = {
        _id: tracking._id,
        userId: tracking.userId,
        token: tracking.token,
      };
      res.status(200).json({ data: data });
    } else {
      return res
        .status(401)
        .json({ message: `You don't have permision call it.` });
    }
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

// @route GET api/tracking/userId/{id}
// @desc Returns a specific tracking data
// @access Public
exports.showByUserId = async function (req, res) {
  try {
    const id = req.params.id;
    // console.log(req.user.role === 'admin' ,req.user, req.user._id === id)
    if (req.user.role === "admin" || req.user._id === id) {
      const tracking = await Tracking.findOne({ userId: new ObjectId(id) });
      if (!tracking)
        return res.status(401).json({ message: "Item does not exist" });
      let data = {
        _id: tracking._id,
        userId: tracking.userId,
        token: tracking.token,
      };
      return res.status(200).json({ data });
    } else {
      return res
        .status(401)
        .json({ message: `You don't have permision call it.` });
    }
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

// @route PUT api/tracking/{id}
// @desc Update tracking details
// @access Public
exports.update = async function (req, res) {
  try {
    const { userId, typeSdk, type } = req.body;
    // Make sure post in role admin
    if (req.user.role !== "admin") {
      return res
        .status(401)
        .json({ message: `You don't have permision update it.` });
    }
    let typeInput = type ? type : "trial";
    let typeSdkInput = typeSdk ? typeSdk : "1";
    let generateTokenData = generateToken(userId + "_" + typeSdkInput);
    const id = req.params.id;
    let payload = {
      userId: userId,
      typeSdk: typeSdkInput,
      token: generateTokenData,
      limit: configLimitCall(typeInput),
      dateExpires: Date.now() + configDayCall(typeInput) * 1000 * 60 * 60 * 24,
    };
    const data = await Tracking.findByIdAndUpdate(
      id,
      { $set: payload },
      { new: true }
    );
    return res
      .status(200)
      .json({ data: data, message: "Tracking has been updated" });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

// @route DESTROY api/tracking/{id}
// @desc Delete Tracking
// @access Public
exports.destroy = async function (req, res) {
  try {
    const id = req.params.id;
    if (req.user.role !== "admin") {
      return res
        .status(401)
        .json({ message: `You don't have permision delete it.` });
    }
    //Make sure the passed id is that of the logged in user
    // if (user_id.toString() !== id.toString()) return res.status(401).json({ message: "Sorry, you don't have the permission to delete this data." });
    await Tracking.findByIdAndDelete(id);
    res.status(200).json({ message: "User has been deleted" });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};
