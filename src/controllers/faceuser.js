const FaceSchema = require("../models/faceuser");
var ObjectId = require("mongoose").Types.ObjectId;
const mongoose = require("mongoose");
module.exports = {
  handleError: (res, reason, message, code) => {
    res.status(code || 500).json({ error: message });
  },
  getUserFaces: async (req, res) => {
    try {
      let data = await FaceSchema.find(
        {
          user_id: mongoose.Types.ObjectId(req.params.userid),
        },
        { updatedAt: 0, __v: 0 }
      );
      let result = {};
      result.data = data;
      res.status(200).json({
        result,
      });
    } catch (e) {
      console.log("e", e);
      module.exports.handleError(res, e, "Failed to get faces.");
    }
  },
  getLastUserFace: async (req, res) => {
    try {
      let data = await FaceSchema.find({
        user_id: mongoose.Types.ObjectId(req.params.userid),
      })
        .sort({ _id: -1 })
        .limit(1);
      let result = {};
      result.data = data;
      res.status(200).json({
        result,
      });
    } catch (e) {
      module.exports.handleError(res, e, "Failed to get last face.");
    }
  },
  saveUserFace: async (data) => {
    try {
      // console.log(" data.facedata", data.facedata, data.facedata.generalResult);
      const newUser = new FaceSchema({
        user_id: mongoose.Types.ObjectId(data.user_id),
        imageInfo: data.facedata.image_info,
        facedatares: data.facedatares,
        generalResult: data.facedata.generalResult,
        specialResult: data.facedata.specialResult,
        generalConclusion: data.facedata.generalConclusion,
        specialConclusion: data.facedata.specialConclusion,
        hintResult: data.facedata.hintResult,
      });
      const dataFace = await newUser.save();
      // console.log("dataFace", dataFace);
      return dataFace;
    } catch (error) {
      console.log("error", error);
    }
  },
  getUserFace: (req, res) => {
    try {
      FaceSchema.findOne(
        { _id: mongoose.Types.ObjectId(req.params.id) },
        function (err, doc) {
          if (err) {
            module.exports.handleError(res, err.message, "Failed to get face");
          } else {
            res.status(200).json(doc);
          }
        }
      );
    } catch (e) {
      module.exports.handleError(res, e, "Failed to get face");
    }
  },
  deleteUserFace: (req, res) => {
    try {
      FaceSchema.deleteOne(
        { _id: mongoose.Types.ObjectId(req.params.id) },
        function (err, result) {
          if (err) {
            module.exports.handleError(
              res,
              err.message,
              "Failed to delete contact"
            );
          } else {
            res.status(200).json(req.params.id);
          }
        }
      );
    } catch (e) {
      module.exports.handleError(res, e, "Failed to delete contact");
    }
  },
  getLastUserFaceSixMonth: async (req, res) => {
    try {
      var d = new Date();
      d.setMonth(d.getMonth() - 6); //6 month ago
      let data = await FaceSchema.find(
        {
          user_id: mongoose.Types.ObjectId(req.params.userid),
          createdAt: { $gte: d },
        },
        {
          imageInfo: 0,
          generalResult: 0,
          specialResult: 0,
          generalConclusion: 0,
          specialConclusion: 0,
          hintResult: 0,
        }
      ).sort({ _id: -1 });
      let result = {};
      result.data = data;
      res.status(200).json({
        result,
      });
    } catch (e) {
      module.exports.handleError(res, e, "Failed to get last face.");
    }
  },
};
