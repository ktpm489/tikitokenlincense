const Keydemo = require("../models/key");

// Create and Save a new Key Data
exports.create = (req, res) => {
  // Validate request
  if (!req.body.key) {
    res.status(400).send({ message: "Content can not be empty!" });
    return;
  }

  // Create a Key Data
  const keyDemo = new Keydemo({
    name: req.body.name,
    key: req.body.key,
  });

  // Save Key Data in the database
  keyDemo
    .save(keyDemo)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Key Data.",
      });
    });
};

// Retrieve all Key Datas from the database.
exports.findAll = (req, res) => {
  const name = req.query.name;
  var condition = name
    ? { name: { $regex: new RegExp(name), $options: "i" } }
    : {};

  Keydemo.find(condition)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving Key Datas.",
      });
    });
};

// Find a single Key Data with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  Keydemo.findById(id)
    .then((data) => {
      if (!data)
        res.status(404).send({ message: "Not found Key Data with id " + id });
      else res.send(data);
    })
    .catch((err) => {
      res
        .status(500)
        .send({ message: "Error retrieving Key Data with id=" + id });
    });
};

// Update a Key Data by the id in the request
exports.update = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Data to update can not be empty!",
    });
  }

  const id = req.params.id;

  Keydemo.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (!data) {
        res.status(404).send({
          message: `Cannot update Key Data with id=${id}. Maybe Key Data was not found!`,
        });
      } else res.send({ message: "Key was updated successfully." });
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error updating Key with id=" + id,
      });
    });
};

// Delete a Key Data with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  Keydemo.findByIdAndRemove(id, { useFindAndModify: false })
    .then((data) => {
      if (!data) {
        res.status(404).send({
          message: `Cannot delete Key Data with id=${id}. Maybe Key Data was not found!`,
        });
      } else {
        res.send({
          message: "Key Data was deleted successfully!",
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Could not delete Key Data with id=" + id,
      });
    });
};

// Delete all Key Datas from the database.
exports.deleteAll = (req, res) => {
  Keydemo.deleteMany({})
    .then((data) => {
      res.send({
        message: `${data.deletedCount} Key Datas were deleted successfully!`,
      });
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all Key Datas.",
      });
    });
};

// Find all published Key Datas
exports.findAllPublished = (req, res) => {
  Keydemo.find({ published: true })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving Key Datas.",
      });
    });
};

// Retrieve all Key Datas  Of User from the database.
exports.findAllByUser = (req, res) => {
  const userId = req.query.userId;
  var condition = userId
    ? { userId: { $regex: new RegExp(userId), $options: "i" } }
    : {};
  Keydemo.find(condition)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving Key Datas.",
      });
    });
};
