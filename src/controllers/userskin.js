const UserSkin = require("../models/userskin");
const FaceUser = require("../controllers/faceuser");
const { uploader, sendEmail, getTypeSdk } = require("../utils/index");
const { parseFaceResult } = require("../utils/parseSkin");
var ObjectId = require("mongoose").Types.ObjectId;
const meitu = require("../functions/meitu/index.js");
const mongoose = require("mongoose");
const db = mongoose.connection;
var ObjectId = require("mongoose").Types.ObjectId;
// @route GET /
// @desc Returns all faceuser
// @access Public
exports.index = async function (req, res) {
  res.status(200).json({ data: "No Data" });
};

// @route POST api/userskin
// @desc Add a new userskin
// @access Public
exports.store = async (req, res) => {
  try {
    const { email } = req.body;
    let typesdk = getTypeSdk(req.headers.apikey);
    if (!typesdk) {
      return res.status(400).send({ success: false, message: "Error api key" });
    }
    const faceResponse = await meitu.detectImage(
      req.body.image_base64,
      typesdk
    );
    if (faceResponse.hasOwnProperty("ErrorMsg")) {
      return res.status(400).send(faceResponse);
    }
    const newUser = new UserSkin({
      email: email,
      facedata: faceResponse,
      typesdk: typesdk,
      token: req.headers.apikey,
    });
    const data = await newUser.save();
    res.status(200).json({ data });
  } catch (error) {
    res.status(500).json({ success: false, message: error.message });
  }
};

// @route GET api/userskin/{id}
// @desc Returns a specific user
// @access Public
exports.show = async function (req, res) {
  try {
    const id = req.params.id;
    // const user = await User.findById(id);
    const user = await UserSkin.findOne({ _id: new ObjectId(id) });
    if (!user)
      return res.status(401).json({ message: "User Skin data does not exist" });

    res.status(200).json({ user });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

// @route GET api/userskin/user/{email}
// @desc Returns a specific user
// @access Public
exports.showByUserEmail = async function (req, res) {
  try {
    if (req.params.email === "") {
      res.status(500).json({ message: "Email is empty" });
    } else {
      const data = await UserSkin.find(
        { email: req.params.email },
        { updatedAt: 0, __v: 0, typesdk: 0 }
      );
      let result = {};
      result.data = data;
      res.status(200).json({
        result,
      });
    }
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

// @route GET api/userskin/user/{email}
// @desc Returns a specific user
// @access Public
exports.showByUserEmailAndQuery = async function (req, res) {
  try {
    if (req.params.email === "") {
      res.status(500).json({ message: "Email is empty" });
    } else {
      // const data = await UserSkin.find({ email : req.params.email }, {updatedAt:0 , __v: 0 , typesdk: 0 })
      // let result = {};
      // result.data = data;
      // res.status(200).json({
      //     result
      // });
      const indexPage = req.query.indexPage || 1;
      const itemPerPage = req.query.itemPerPage || 3;
      const order = req.query.sort || "_id";
      const direction = req.query.direction == "asc" ? 1 : -1;
      let sort = {};
      sort[order] = direction;
      let conds = null;
      if (req.query.typesdk) {
        let obj = {
          typesdk: req.query.typesdk,
          email: req.params.email,
        };
        if (req.headers.apikey) {
          obj.token = req.headers.apikey;
        }

        conds = {
          $and: [obj],
        };
      } else {
        let obj = {
          email: req.params.email,
        };
        if (req.headers.apikey) {
          obj.token = req.headers.apikey;
        }
        conds = obj;
      }
      // const data = await UserSkin.find(conds, null, null).sort(sort);
      const data = await UserSkin.find(
        conds,
        { token: 0 },
        {
          skip: (indexPage - 1) * itemPerPage,
          limit: parseInt(itemPerPage),
        }
      ).sort(sort);
      const count = await UserSkin.countDocuments(conds);
      let result = {};
      result.data = data;
      result.indexPage = indexPage;
      result.totalItems = count;
      result.itemPerPage = Math.ceil(count / itemPerPage);
      res.json({
        data: result,
      });
    }
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

// @route GET api/userskin/user/{email}
// @desc Returns a specific user
// @access Public
exports.showById = async function (req, res) {
  // console.log("req.params", req.params);
  const id = req.params.id;
  try {
    if (id === "") {
      res.status(500).json({ message: "Id is empty" });
    } else {
      const data = await UserSkin.findOne(
        { _id: new ObjectId(id) },
        {
          updatedAt: 0,
          __v: 0,
          typesdk: 0,
          "facedata.generalConclusion": 0,
          "facedata.specialConclusion": 0,
          "facedata.hintResult": 0,
        }
      );
      let result = {};
      result.data = data;
      res.status(200).json({
        result,
      });
    }
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

//store v2 add face skin save
exports.storeV2 = async (req, res) => {
  try {
    const { email, userId = "000000000000000000000000" } = req.body;
    // let typesdk = getTypeSdk(req.headers.apikey);
    // if (!typesdk) {
    //   return res.status(400).send({ success: false, message: "Error api key" });
    // }
    const faceResponse = await meitu.detectImageV2(req.body.image_base64);
    if (faceResponse.hasOwnProperty("ErrorMsg")) {
      return res.status(400).send(faceResponse);
    }
    // const newUser = new UserSkin({
    //   email: email,
    //   facedata: faceResponse[0],
    //   typesdk: typesdk,
    //   token: req.headers.apikey,
    // });
    // const data = await newUser.save();
    // call faceskin save data
    // console.log("faceResponse", faceResponse);
    const resultSaveFace = await FaceUser.saveUserFace({
      user_id: userId,
      facedata: faceResponse[0],
      facedatares: faceResponse[1],
    });
    // console.log("resultSaveFace", resultSaveFace);
    res.status(200).json({
      data: {
        email: email,
        facedata: faceResponse[0],
        facedatares: faceResponse[1],
      },
    });
  } catch (error) {
    res.status(500).json({ success: false, message: error.message });
  }
};


//store v3 add face skin save
exports.storeV3 = async (req, res) => {
  try {
    const { email, userId = "000000000000000000000000" } = req.body;
    // let typesdk = getTypeSdk(req.headers.apikey);
    // if (!typesdk) {
    //   return res.status(400).send({ success: false, message: "Error api key" });
    // }
    const faceResponse = await meitu.detectImageV3(req.body.image_base64);
    if (faceResponse.hasOwnProperty("ErrorMsg")) {
      return res.status(400).send(faceResponse);
    }
    // const newUser = new UserSkin({
    //   email: email,
    //   facedata: faceResponse[0],
    //   typesdk: typesdk,
    //   token: req.headers.apikey,
    // });
    // const data = await newUser.save();
    // call faceskin save data
    // console.log("faceResponse", faceResponse);
    const resultSaveFace = await FaceUser.saveUserFace({
      user_id: userId + "",
      facedata: faceResponse[0],
      facedatares: faceResponse[1],
    });
    // console.log("resultSaveFace", resultSaveFace);
    res.status(200).json({
      data: {
        email: email,
        facedata: faceResponse[0],
        facedatares: faceResponse[1],
      },
    });
  } catch (error) {
    res.status(500).json({ success: false, message: error.message });
  }
};



//store outsource  add face skin save
exports.storeOutSource = async (req, res) => {
  try {
    const { email, userId = "000000000000000000000000" } = req.body;
    const faceResponse = await meitu.detectImageV3(req.body.image_base64);
    if (faceResponse.hasOwnProperty("ErrorMsg")) {
      return res.status(400).send(faceResponse);
    }
    const resultSaveFace = await FaceUser.saveUserFace({
      user_id: userId + "",
      facedata: faceResponse[0],
      facedatares: faceResponse[1],
    });
    // console.log("resultSaveFace", resultSaveFace);
    res.status(200).json({
      data: {
        email: email,
        facedata: faceResponse[0],
        facedatares: faceResponse[1],
        // rawData : faceResponse[2]
      },
    });
  } catch (error) {
    res.status(500).json({ success: false, message: error.message });
  }
};

exports.storeOutSourceV2 = async (req, res) => {
  try {
    const { email, userId = "000000000000000000000000" } = req.body;
    const faceResponse = await meitu.detectImageOutsourceV2(req.body.image_base64);
    if (faceResponse.hasOwnProperty("ErrorMsg")) {
      return res.status(400).send(faceResponse);
    }
    const resultSaveFace = await FaceUser.saveUserFace({
      user_id: userId + "",
      facedata: faceResponse[0],
      facedatares: faceResponse[1],
    });
    // console.log("resultSaveFace", resultSaveFace);
    res.status(200).json({
      data: {
        email: email,
        facedata: faceResponse[0],
        facedatares: faceResponse[1],
        rawData : faceResponse[2]
      },
    });
  } catch (error) {
    res.status(500).json({ success: false, message: error.message });
  }
};


exports.storeOutSourcePenisila = async (req, res) => {
  try {
    const { email, userId = "000000000000000000000000" } = req.body;
    const faceResponse = await meitu.detectImageOutsourcePenisila(req.body.image_base64);
    if (faceResponse.hasOwnProperty("ErrorMsg")) {
      return res.status(400).send(faceResponse);
    }
    const resultSaveFace = await FaceUser.saveUserFace({
      user_id: userId + "",
      facedata: faceResponse[0],
      facedatares: faceResponse[1],
    });
    // console.log("resultSaveFace", resultSaveFace);
    res.status(200).json({
      data: {
        email: email,
        facedata: faceResponse[0],
        facedatares: faceResponse[1],
        // rawData : faceResponse[2]
      },
    });
  } catch (error) {
    res.status(500).json({ success: false, message: error.message });
  }
};
