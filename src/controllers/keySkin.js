const Keydemo = require("../models/keySkin");
var ObjectId = require('mongoose').Types.ObjectId
// Create and Save a new Key Data
exports.create = async (req, res) => {
  // Validate request
  try {
    if (!req.body.key) {
      res.status(400).send({ message: "Content can not be empty!" });
      return;
    }
    const db = await Keydemo.find()
    // Create a Key Data
   
    if(db.length == 0){
      const meitu = new Keydemo({
        key: req.body.key
      });
      const response = await meitu.save()
  
      res.json({
        status: 200,
        message: "Success",
        data: response
      })
      return
    }
    const update = {
      key: req.body.key
    }
    await Keydemo.updateOne({_id: ObjectId(db[0]['_id'])}, update)
    const response = await Keydemo.find()
    
    res.json({
      status: 200,
      message: "Success",
      data: response[0].key
    }) 
  }
  catch(e){
    res.json({
      status: 201,
      message: e.message,
      data: null
    });
  }
};

// Retrieve all Key Datas from the database.
exports.findData = async (req, res) => {
    try {
      const response = await Keydemo.find()
      res.json({
        status: 200,
        message: "Success",
        data: response[0].key
      })
    }catch(e) {
        res.json({
          status: 201,
          message: "Error",
          data: null
        });
    }
};

