require("dotenv").config();

const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const passport = require("passport");
const bodyParser = require("body-parser");
const path = require("path");

// Setting up port
// const connUri = process.env.MONGO_LOCAL_CONN_URL;
const connUri = "mongodb://admin:admin2021@207.148.119.166/makeupdb";
let PORT = process.env.PORT || 3000;

//=== 1 - CREATE APP
// Creating express app and configuring middleware needed for authentication
const app = express();

app.use(cors());

// for parsing application/json
// app.use(express.json());

// for parsing application/xwww-
// app.use(express.urlencoded({ extended: false }));
//form-urlencoded

// limit 50mb
// app.use(express.urlencoded({ extended: true }))
app.use(express.json({ limit: "1000mb", extended: true }));
app.use(express.urlencoded({ limit: "1000mb", extended: false }));

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "jade");

//=== 2 - SET UP DATABASE
//Configure mongoose's promise to global promise
mongoose.promise = global.Promise;

mongoose.connect(connUri, { useNewUrlParser: true, useCreateIndex: true });

const connection = mongoose.connection;
connection.once("open", () =>
  console.log("MongoDB --  database connection established successfully!")
);
connection.on("error", (err) => {
  console.log(
    "MongoDB connection error. Please make sure MongoDB is running. " + err
  );
  process.exit();
});

//=== 3 - INITIALIZE PASSPORT MIDDLEWARE
app.use(passport.initialize());
require("./middlewares/jwt")(passport);

//=== 4 - CONFIGURE ROUTES
//Configure Route
require("./routes/index")(app);

//=== 5 - START SERVER
var server = app.listen(PORT, () =>
  console.log("Server running on http://localhost:" + PORT + "/")
);
server.setTimeout(5000000);
