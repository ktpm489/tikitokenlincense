const {
  processAginSkinResult,
  processAcneResult,
  processDarkCircleResult,
  processPoreResult,
  processSkinSpotResult,
} = require("./skin");

const obj = {
  media_info_list: [
    {
      media_extra: {
        faces: [
          {
            face_attributes: {
              BlackHead_Area: {
                confidence: 1,
                value: "0.003009",
              },
              CrowsFeed_LeftArea: {
                confidence: 1,
                value: "0.000000",
              },
              CrowsFeed_RightArea: {
                confidence: 1,
                value: "0.000000",
              },
              EyeWrinkle_LeftArea: {
                confidence: 1,
                value: "0.000000",
              },
              EyeWrinkle_RightArea: {
                confidence: 1,
                value: "0.000000",
              },
              NasolabialFolds_LeftArea: {
                confidence: 1,
                value: "0.000000",
              },
              NasolabialFolds_RightArea: {
                confidence: 1,
                value: "0.000000",
              },
              PoresBetweenBrow_Have: {
                confidence: 1,
                value: "0",
              },
              PoresCheeks_Left: {
                confidence: 1,
                value: "0",
              },
              PoresCheeks_Right: {
                confidence: 1,
                value: "0",
              },
              PoresForehead_Have: {
                confidence: 1,
                value: "0",
              },
              SkinAcne: {
                confidence: 0.5,
                value: "0",
              },
              SkinAge: {
                confidence: 1,
                value: "23",
              },
              SkinBlackHeads: {
                confidence: 1,
                rectangle: [
                  {
                    height: 2,
                    left: 270,
                    top: 321,
                    width: 2,
                  },
                  {
                    height: 1,
                    left: 276,
                    top: 321,
                    width: 2,
                  },
                  {
                    height: 2,
                    left: 237,
                    top: 328,
                    width: 2,
                  },
                ],
                value: "3",
              },
              SkinColorHueDelta: {
                confidence: 1,
                value: "2",
              },
              SkinColorLevel: {
                confidence: 1,
                value: "4",
              },
              SkinCrowsFeed_Left: {
                confidence: 1,
                value: "0",
              },
              SkinCrowsFeed_Right: {
                confidence: 1,
                value: "0",
              },
              SkinEyeFineLineScore_Left: {
                confidence: 1,
                value: "0.000859",
              },
              SkinEyeFineLineScore_Right: {
                confidence: 1,
                value: "0.001205",
              },
              SkinEyeFineLine_Left: {
                confidence: 1,
                value: "0",
              },
              SkinEyeFineLine_Right: {
                confidence: 1,
                value: "0",
              },
              SkinEyeWrinkle_Left: {
                confidence: 1,
                value: "0",
              },
              SkinEyeWrinkle_Right: {
                confidence: 1,
                value: "0",
              },
              SkinForeHeadWrinkle: {
                confidence: 1,
                value: "0",
              },
              SkinForeHeadWrinkleArea: {
                confidence: 1,
                value: "0.000000",
              },
              SkinHighlight: {
                confidence: 1,
                value: "0",
              },
              SkinHighlightPrec: {
                confidence: 1,
                value: "0.000000",
              },
              SkinLevel: {
                confidence: 1,
                value: "66",
              },
              SkinMole: {
                confidence: 0.5,
                value: "0",
              },
              SkinNasolabialFolds_Left: {
                confidence: 1,
                value: "0",
              },
              SkinNasolabialFolds_Right: {
                confidence: 1,
                value: "0",
              },
              SkinPandaEye_Left: {
                confidence: 1,
                value: "0",
              },
              SkinPandaEye_Left_Artery: {
                confidence: 1,
                value: "0",
              },
              SkinPandaEye_Left_Pigment: {
                confidence: 1,
                value: "0",
              },
              SkinPandaEye_Left_Shadow: {
                confidence: 1,
                value: "0",
              },
              SkinPandaEye_Right: {
                confidence: 1,
                value: "0",
              },
              SkinPandaEye_Right_Artery: {
                confidence: 1,
                value: "2",
              },
              SkinPandaEye_Right_Pigment: {
                confidence: 1,
                value: "0",
              },
              SkinPandaEye_Right_Shadow: {
                confidence: 1,
                value: "0",
              },
              SkinPimple: {
                confidence: 0.5,
                value: "0",
              },
              SkinRosaceaChin: {
                confidence: 1,
                value: "0",
              },
              SkinRosaceaForehead: {
                confidence: 1,
                value: "0",
              },
              SkinRosaceaLeftcheek: {
                confidence: 1,
                value: "10",
              },
              SkinRosaceaNose: {
                confidence: 1,
                value: "0",
              },
              SkinRosaceaRightcheek: {
                confidence: 1,
                value: "10",
              },
              SkinSpot: {
                confidence: 0.5,
                value: "1",
              },
              SkinType: {
                confidence: 1,
                value: "1",
              },
            },
            face_rectangle: {
              height: 215.7991,
              left: 110.16174,
              top: 165.86742,
              width: 215.7991,
            },
          },
        ],
      },
    },
  ],
  parameter: {},
};
const data = obj.media_info_list[0].media_extra.faces[0].face_attributes;
//  K1, K2, K3, K4 General
// K1 - Skin Age Detection
const skinAgeArr = ["SkinAge"];
const skinAgeTrans = {
  title: { en: "Skin Age Detection", vi: "Nhận diện tuổi da" },
  descript: {
    en: "Skin age does not necessarily reveal the user’s actual age, but is a great indicator of their health and lifestyle. If the skin age is older than the actual age, it is a sign that users should begin to change their daily routine and skin care.",
    vi: "Tuổi da không nhất thiết tiết lộ tuổi thực của người dùng, nhưng là một chỉ số tuyệt vời về sức khỏe và lối sống của họ. Nếu tuổi da lớn hơn tuổi thực, đó là dấu hiệu để người dùng bắt đầu thay đổi các thói quen sinh hoạt và chăm sóc da của họ.",
  },
  SkinAge: { en: "Skin Age", vi: "Tuổi da", value: "normal" },
};
// K2 - Skin Type Identification
const skinTypeArr = ["SkinType", "SkinHighlight", "SkinHighlightPrec"];
const skinTypeTrans = {
  title: { en: "Skin Type Identification", vi: "Nhận diện các loại da" },
  descript: {
    en: "Having a beautiful skin begins with knowing your skin type. Scientifically speaking, there are four skin types: oily, dry, normal, and a combination of these three. We identify the skin type found on different areas of the face as well as specific problems that may arise.",
    vi: "Nhận biết chính xác loại da của mình là bước đầu tiên trong việc chăm sóc da. Có bốn loại da cơ bản sau đây: da dầu, da khô, da thường và da hỗn hợp là sự kết hợp của ba loại da này. Chúng tôi xác định loại da được tìm thấy trên các vùng khác nhau của khuôn mặt cũng như các vấn đề cụ thể có thể phát sinh.",
  },
  SkinType: {
    en: "Skin Type",
    vi: "Loại da",
    value: "range",
    valueEN: {
      0: "Oily",
      1: "Dry",
      2: "Normal",
      3: "Combination",
    },
    valueVI: {
      0: "Da dầu",
      1: "Da khô",
      2: "Da trung tính",
      3: "Da hỗn hợp",
    },
  },
  SkinHighlight: {
    en: "The number of oily highlight area",
    vi: "Vùng da dầu",
    value: "normal",
  },
  SkinHighlightPrec: {
    en: "The percentage of oily highlight area (from 0 to 1)",
    vi: "Tỷ lệ diện tích Vùng da dầu (từ 0 -> 1)",
    value: "normal",
  },
};
// K3- Skin Tone Detection
const skinToneArr = ["SkinColorHueDelta", "SkinColorLevel", "SkinLevel"];
const skinToneTrans = {
  title: { en: "Skin Tone Detection", vi: "Nhận diện tông màu da" },
  descript: {
    en: "According to international standards, there are 40 different levels of skin tone: our technology can precisely detect the level of each individual user. Morever, we also provide the Pantone SkinTone color, which will help users to easily match and coordinate cosmetics to skin color.",
    vi: "Theo tiêu chuẩn quốc tế, có 40 mức độ khác nhau của tông màu da: công nghệ của chúng tôi có thể phát hiện chính xác tông màu da của mỗi người dùng. Ngoài ra, chúng tôi còn cung cấp thông tin về cấp độ tông màu da theo bảng màu Pantone 110, giúp người dùng có thể dễ dàng hơn trong việc tìm ra những màu sắc phù hợp nhất với làn da của mình.",
  },
  SkinColorHueDelta: {
    en: "Skin Color Level",
    vi: "Mức độ màu da",
    value: "range",
    valueEN: {
      0: "Neutral",
      1: "yellow",
      2: "Reddish",
    },
    valueVI: {
      0: "Trung tính",
      1: "Vàng",
      2: "Ứng Đỏ",
    },
  },
  SkinLevel: {
    en: "Level of skin tone according to Pantone",
    vi: "Cấp độ tông màu da theo Pantone",
    value: "normal",
  },
  SkinColorLevel: {
    en: "Level of skin tone brightness",
    vi: "Phân tích độ sáng tông màu da",
    value: "range",
    valueEN: {
      1: "Clear white",
      2: "White",
      3: "Natural",
      4: "Wheat",
      5: "Dark",
      6: "Swarthy",
    },
    valueVI: {
      1: "Rất Trắng",
      2: "Trắng",
      3: "Tự Nhiên",
      4: "Vàng",
      5: "Ngăm Đen",
      6: "Nhiều Màu",
    },
  },
};
// K4- Skin Mole  Detection
const skinMoleArr = ["SkinMole"];
const skinMoleTrans = {
  title: { en: "Skin Mole Detection", vi: "Nhận diện nốt ruồi" },
  SkinMole: { en: "Skin Mole", vi: "Nốt Ruồi", value: "normal" },
  descript: {
    en: "Skin moles are common, almost everyone has them. Knowing the difference between moles and other spots is vital for knowing which to pay more attention to.",
    vi: "Nốt ruồi trên da là một tình trạng phổ biến mà hầu hết mọi người đều có. Phân biệt sự khác nhau giữa nốt ruồi và các vết nám và thâm khác trên da là điều cần thiết để có sự quan tâm đúng đắn cho từng loại.",
  },
};

// K5 - Signs of Aging Skin Detection  -Special
const agingSkinArr = [
  "SkinForeHeadWrinkle",
  "SkinForeHeadWrinkleArea",
  "SkinEyeFineLine_Left",
  "SkinEyeFineLine_Right",
  "SkinEyeFineLineScore_Left",
  "SkinEyeFineLineScore_Right",
  "SkinCrowsFeed_Left",
  "SkinCrowsFeed_Right",
  "SkinNasolabialFolds_Left",
  "SkinNasolabialFolds_Right",
  "NasolabialFolds_LeftArea",
  "NasolabialFolds_RightArea",
  "SkinEyeWrinkle_Left",
  "SkinEyeWrinkle_Right",
  "EyeWrinkle_LeftArea",
  "EyeWrinkle_RightArea",
  "CrowsFeed_LeftArea",
  "CrowsFeed_RightArea",
];
const agingSkinTrans = {
  title: {
    en: "Signs of Aging Skin Detection",
    vi: "Nhận diện các dấu hiệu Lão Hóa Da",
  },
  descript: {
    en: "After identifying different types of facial wrinkles including forehead wrinkles, crow's feet, eye wrinkles, smile lines, and fine lines around the eye, we are able to show users the level of impact and provide more reliable data according to their specific situation.",
    vi: "Sau khi xác định các loại nếp nhăn khác nhau trên khuôn mặt bao gồm nếp nhăn trán, vết chân chim, nếp nhăn mắt, đường cười và nếp nhăn quanh mắt, chúng tôi có thể cho người dùng thấy mức độ tác động và cung cấp dữ liệu đáng tin cậy hơn tùy theo tình hình cụ thể của họ.",
  },
  SkinForeHeadWrinkle: {
    en: "Skin ForeHead Wrinkle",
    vi: "Nếp nhăn trên trán",
    value: "range",
    valueEN: { 0: "No", 1: "Yes" },
    valueVI: { 0: "Không ", 1: " Có" },
  },
  SkinForeHeadWrinkleArea: {
    en: "Skin ForeHead Wrinkle Area",
    vi: "Tỷ lệ nếp nhăn trên trán (0->1)",
    value: "normal",
  },
  SkinEyeFineLine_Left: {
    en: "Skin Eye Fine Line Left",
    vi: "Nếp nhăn mắt Trái",
    value: "range",
    valueEN: { 0: "No", 1: "Yes" },
    valueVI: { 0: "Không ", 1: " Có" },
  },
  SkinEyeFineLine_Right: {
    en: "Skin Eye Fine Line Right",
    vi: "Nếp nhăn mắt Phải",
    value: "range",
    valueEN: { 0: "No", 1: "Yes" },
    valueVI: { 0: "Không ", 1: " Có" },
  },
  SkinEyeFineLineScore_Left: {
    en: "Skin Eye Fine Line Score Left (0 -> 1)",
    vi: "Kết quả phân tích nếp nhăn mắt trái (0->1)",
    value: "normal",
  },
  SkinEyeFineLineScore_Right: {
    en: "Skin Eye Fine Line Score Right (0 -> 1)",
    vi: "Kết quả phân tích nếp nhăn mắt phải (0->1)",
    value: "normal",
  },
  SkinCrowsFeed_Left: {
    en: "Skin Crows Feed Left",
    vi: "Vết chân chim trên mắt trái",
    value: "range",
    valueEN: { 0: "No", 1: "Yes" },
    valueVI: { 0: "Không ", 1: " Có" },
  },
  SkinCrowsFeed_Right: {
    en: "Skin Crows Feed Right",
    vi: "Vết chân chim trên mắt phải",
    value: "range",
    valueEN: { 0: "No", 1: "Yes" },
    valueVI: { 0: "Không ", 1: " Có" },
  },
  SkinNasolabialFolds_Left: {
    en: "Skin Nasolabial Folds Left",
    vi: "Nếp gấp da mũi trái",
    value: "range",
    valueEN: { 0: "No", 1: "Yes" },
    valueVI: { 0: "Không ", 1: " Có" },
  },
  SkinNasolabialFolds_Right: {
    en: "Skin Nasolabial Folds Right",
    vi: "Nếp gấp da mũi phải",
    value: "range",
    valueEN: { 0: "No", 1: "Yes" },
    valueVI: { 0: "Không ", 1: " Có" },
  },
  NasolabialFolds_LeftArea: {
    en: "Nasolabial Folds Left Area (0 -> 1)",
    vi: "Kết quả phân tích nếp gấp da mũi trái (0->1)",
    value: "normal",
  },
  NasolabialFolds_RightArea: {
    en: "Nasolabial Folds Right Area (0 -> 1)",
    vi: "Kết quả phân tích nếp gấp da mũi trái (0->1)",
    value: "normal",
  },
  SkinEyeWrinkle_Left: {
    en: "Skin Eye Wrinkle Left",
    vi: "Da mắt trái nhăn",
    value: "range",
    valueEN: { 0: "No", 1: "Yes" },
    valueVI: { 0: "Không ", 1: " Có" },
  },
  SkinEyeWrinkle_Right: {
    en: "Skin Eye Wrinkle Left",
    vi: "Da mắt phải nhăn",
    value: "range",
    valueEN: { 0: "No", 1: "Yes" },
    valueVI: { 0: "Không ", 1: " Có" },
  },
  EyeWrinkle_LeftArea: {
    en: "Eye Wrinkle Left Area (0 -> 1)",
    vi: "Tỷ lệ vùng - Da mắt trái nhăn (0->1)",
    value: "normal",
  },
  EyeWrinkle_RightArea: {
    en: "Eye Wrinkle Right Area (0 -> 1)",
    vi: "Tỷ lệ vùng - Da mắt phải nhăn (0->1)",
    value: "normal",
  },
  CrowsFeed_LeftArea: {
    en: "CrowsFeed Left Area (0 -> 1)",
    vi: "Tỷ lệ diện tích vết chân chim mắt trái (0->1)",
    value: "normal",
  },
  CrowsFeed_RightArea: {
    en: "CrowsFeed Right Area (0 -> 1)",
    vi: "Tỷ lệ diện tích vết chân chim mắt phải (0->1)",
    value: "normal",
  },
};
// K6 - Blackhead And Skin Rosacea Detection - Special
const acneArr = [
  "SkinBlackHeads",
  "BlackHead_Area",
  "SkinPimple",
  "SkinAcne",
  "SkinRosaceaNose",
  "SkinRosaceaLeftcheek",
  "SkinRosaceaRightcheek",
  "SkinRosaceaForehead",
  "SkinRosaceaChin",
];
const acneTrans = {
  title: {
    en: "Acne And Skin Rosacea Detection",
    vi: "Nhận diện các vấn đề về mụn và viêm da mẫn đỏ",
  },
  descript: {
    en: "We can accurately identify symptoms of acne such as pimples, blackheads and acne vulgaris from mild to severe. Especially, with blackheads that would not normally be seen without some kind of magnification, our high-resolution blackhead detection technology can help locate even the most minute blackheads to ensure they have nowhere to hide.Rosacea is a long-term skin condition that causes redness and visible blood vessels in your face.And we can early detection of rosacea can help user to get an appropriate treatment before redness and swelling can get worse and might become permanent.",
    vi: "Chúng tôi sẽ nhận diện được chính xác các triệu chứng của mụn, bao gồm mụn dầu đen, các nốt mụn và sẹo mụn ở mức độ từ nhẹ đến nặng. Đặc biệt, với các nốt mụn đầu đen mà thông thường sẽ không thể nhìn thấy nếu không có một số loại phóng đại , công nghệ phát hiện mụn đầu đen có độ phân giải cao của chúng tôi sẽ giúp nhận diện được vị trí ngay cả những mụn đầu đen nhỏ nhất để đảm bảo chúng không có nơi nào để ẩn náu.Và Tình trạng da viêm mẫn đỏ là một tổn thương da lâu dài gây ra sự mẫn đỏ và khiến các mạch máu nổi rõ trên khuôn mặt. Nhận diện sớm tình trạng da viêm mẫn đỏ có thể giúp người dùng có những biện pháp điều trị kịp thời trước khi tình trạng chuyển biến nghiêm trọng và có thể trở thành vĩnh viễn.",
  },
  SkinBlackHeads: {
    en: "The number of blackheads",
    vi: "Số lượng mụn đầu đen",
    value: "normal",
  },
  BlackHead_Area: {
    en: "The proportion of the area occupied by blackheads (0->1)",
    vi: "Tỷ lệ vùng da mặt bị mụn đầu đen (0->1)",
    value: "normal",
  },
  SkinPimple: {
    en: "The number of acne scar",
    vi: "Số lượng sẹo mụn",
    value: "normal",
  },
  SkinAcne: {
    en: "The number of acne",
    vi: "Số lượng sẹo mụn",
    value: "normal",
  },
  SkinRosaceaNose: {
    en: "Skin Rosacea Nose ",
    vi: "Da viêm mẫn đỏ - Quanh mũi",
    value: "range",
    valueEN: { 0: 'No rosacea around the nose"', 1: "Rosacea around the nose" },
    valueVI: {
      0: "Không bị viêm da mẫn đỏ quanh mũi  ",
      1: "Xuất hiện viêm da mẫn đỏ quanh mũi",
    },
  },
  SkinRosaceaLeftcheek: {
    en: "Skin Rosacea Left Cheek",
    vi: "Da viêm mẫn đỏ - Má trái",
    value: "range",
    valueEN: {
      0: "No rosacea on the left cheek",
      1: "Rosacea on the left cheek",
    },
    valueVI: {
      0: "Không có da viêm mẫn đỏ ở má trái",
      1: "Xuất hiện da viêm mẫn đỏ ở má trái",
    },
  },
  SkinRosaceaRightcheek: {
    en: "Skin Rosacea Right Cheek",
    vi: "Da viêm mẫn đỏ - Má phải",
    value: "range",
    valueEN: {
      0: "No rosacea on the right cheek",
      1: "Rosacea on the right cheek",
    },
    valueVI: {
      0: "Không có da viêm mẫn đỏ ở má phải",
      1: "Xuất hiện da viêm mẫn đỏ ở má phải",
    },
  },
  SkinRosaceaForehead: {
    en: "Skin Rosacea Forehead",
    vi: "Da viêm mẫn đỏ - Trán",
    value: "range",
    valueEN: { 0: "No rosacea on the forehead", 1: "Rosacea on the forehead" },
    valueVI: {
      0: "Không bị da viêm mẫn đỏ ở trên trán",
      1: " Xuất hiện da viêm mẫn đỏ trên trán",
    },
  },
  SkinRosaceaChin: {
    en: "Skin Rosacea Chin",
    vi: "Da viêm mẫn đỏ - Cằm",
    value: "range",
    valueEN: { 0: "No rosacea on the chin", 1: "Rosacea on the chin" },
    valueVI: {
      0: "Không có da viêm mẫn đỏ ở cằm",
      1: " Xuất hiện da viêm mẫn đỏ ở cằm",
    },
  },
};
// K7 - Dark Circle Detection - Special
const darkCircleArr = [
  "SkinPandaEye_Left",
  "SkinPandaEye_Right",
  "SkinPandaEye_Left_Pigment",
  "SkinPandaEye_Right_Pigment",
  "SkinPandaEye_Left_Artery",
  "SkinPandaEye_Right_Artery",
  "SkinPandaEye_Left_Shadow",
  "SkinPandaEye_Right_Shadow",
];
const darkCircleTrans = {
  title: {
    en: "Dark Circle Detection",
    vi: "Nhận diện các vấn đề Quầng thâm mắt",
  },
  descript: {
    en: "In fact, the skin that surrounds your eyes is thinner and more delicate than that on the rest of your face, that's why  dark circles and puffines are more likely to occur. We can scientifically identify the type of dark circles (pigmented or vascular) as well as their level of impact.",
    vi: "Trên thực tế, vùng da xung quanh mắt mỏng hơn vùng còn lại của da mặt, điều này khiến các tình trạng như quầng thâm và bọng mắt dễ xảy ra hơn. Chúng tôi sẽ nhận diện một cách khoa học các vấn đề về quầng thâm mà bạn đang gặp phải (do sắc tố hay do mạch máu) cũng như xác định mức độ nghiêm trọng của chúng.",
  },
  SkinPandaEye_Left: {
    en: "Skin PandaEye Left",
    vi: "Quầng thâm - mắt trái",
    value: "range",
    valueEN: { 0: "No", 1: "Yes" },
    valueVI: { 0: "Không ", 1: " Có" },
  },
  SkinPandaEye_Right: {
    en: "Skin PandaEye Right",
    vi: "Quầng thâm - mắt phải",
    value: "range",
    valueEN: { 0: "No", 1: "Yes" },
    valueVI: { 0: "Không ", 1: " Có" },
  },
  SkinPandaEye_Left_Pigment: {
    en: "Skin PandaEye Left Pigment",
    vi: "Quầng thâm do sắc tố - Mắt Trái",
    value: "range",
    valueEN: { 0: "No", 1: "Light", 2: "Medium", 3: "Severe" },
    valueVI: { 0: "Không", 1: "Nhẹ", 2: "Trung bình", 3: "Nặng" },
  },
  SkinPandaEye_Right_Pigment: {
    en: "Skin PandaEye Right Pigment",
    vi: "Quầng thâm do sắc tố - Mắt Phải",
    value: "range",
    valueEN: { 0: "No", 1: "Light", 2: "Medium", 3: "Severe" },
    valueVI: { 0: "Không", 1: "Nhẹ", 2: "Trung bình", 3: "Nặng" },
  },
  SkinPandaEye_Left_Artery: {
    en: "Skin PandaEye Left Artery",
    vi: "Quầng thâm do mạch máu - Mắt trái",
    value: "range",
    valueEN: { 0: "No", 1: "Light", 2: "Medium", 3: "Severe" },
    valueVI: { 0: "Không", 1: "Nhẹ", 2: "Trung bình", 3: "Nặng" },
  },
  SkinPandaEye_Right_Artery: {
    en: "Skin PandaEye Right Artery",
    vi: "Quầng thâm do mạch máu - Mắt phải",
    value: "range",
    valueEN: { 0: "No", 1: "Light", 2: "Medium", 3: "Severe" },
    valueVI: { 0: "Không", 1: "Nhẹ", 2: "Trung bình", 3: "Nặng" },
  },
  SkinPandaEye_Left_Shadow: {
    en: "Skin PandaEye Left Shadow",
    vi: "Quầng thâm do Bọng dưới mắt - Mắt trái",
    value: "range",
    valueEN: { 0: "No", 1: "Light", 2: "Medium", 3: "Severe" },
    valueVI: { 0: "Không", 1: "Nhẹ", 2: "Trung bình", 3: "Nặng" },
  },
  SkinPandaEye_Right_Shadow: {
    en: "Skin PandaEye Right Shadow",
    vi: "Quầng thâm do Bọng dưới mắt - Mắt phải",
    value: "range",
    valueEN: { 0: "No", 1: "Light", 2: "Medium", 3: "Severe" },
    valueVI: { 0: "Không", 1: "Nhẹ", 2: "Trung bình", 3: "Nặng" },
  },
};

// K8 merge with K6 - Skin Rosacea Detection -Special
// const rosaceaArr = ['SkinRosaceaNose', 'SkinRosaceaLeftcheek','SkinRosaceaRightcheek','SkinRosaceaForehead','SkinRosaceaChin']
// const rosaceaTrans = {
//     'title' : {'en' : 'Skin Rosacea Detection' ,'vi' : 'Nhận diện Da viêm mẫn đỏ'},
//     'descript' : {'en' : 'Rosacea is a long-term skin condition that causes redness and visible blood vessels in your face. Early detection of rosacea can help user to get an appropriate treatment before redness and swelling can get worse and might become permanent.' , 'vi' : 'Tình trạng da viêm mẫn đỏ là một tổn thương da lâu dài gây ra sự mẫn đỏ và khiến các mạch máu nổi rõ trên khuôn mặt. Nhận diện sớm tình trạng da viêm mẫn đỏ có thể giúp người dùng có những biện pháp điều trị kịp thời trước khi tình trạng chuyển biến nghiêm trọng và có thể trở thành vĩnh viễn.'},
//     'SkinRosaceaNose' : {'en' : 'Skin Rosacea Nose ' ,'vi' : 'Da viêm mẫn đỏ - Quanh mũi',  'value' :'range', 'valueEN' :{ '0' : 'No rosacea around the nose"', '1' :'Rosacea around the nose'} , 'valueVI' : {'0' :'Không bị viêm da mẫn đỏ quanh mũi  ' , '1' :'Xuất hiện viêm da mẫn đỏ quanh mũi'} },
//     'SkinRosaceaLeftcheek' : {'en' : 'Skin Rosacea Left Cheek' ,'vi' : 'Da viêm mẫn đỏ - Má trái',  'value' :'range', 'valueEN' :{ '0' : 'No rosacea on the left cheek', '1' :'Rosacea on the left cheek'} , 'valueVI' : {'0' :'Không có da viêm mẫn đỏ ở má trái' , '1' :'Xuất hiện da viêm mẫn đỏ ở má trái'} },
//     'SkinRosaceaRightcheek' : {'en' : 'Skin Rosacea Right Cheek' ,'vi' : 'Da viêm mẫn đỏ - Má phải',  'value' :'range', 'valueEN' :{ '0' : 'No rosacea on the right cheek', '1' :'Rosacea on the right cheek'} , 'valueVI' : {'0' :'Không có da viêm mẫn đỏ ở má phải' , '1' :'Xuất hiện da viêm mẫn đỏ ở má phải'} },
//     'SkinRosaceaForehead' : {'en' : 'Skin Rosacea Forehead' ,'vi' : 'Da viêm mẫn đỏ - Trán',  'value' :'range', 'valueEN' :{ '0' : 'No rosacea on the forehead', '1' :'Rosacea on the forehead'} , 'valueVI' : {'0' :'Không bị da viêm mẫn đỏ ở trên trán' , '1' :' Xuất hiện da viêm mẫn đỏ trên trán'} },
//     'SkinRosaceaChin' : {'en' : 'Skin Rosacea Chin' ,'vi' : 'Da viêm mẫn đỏ - Cằm',  'value' :'range', 'valueEN' :{ '0' : 'No rosacea on the chin', '1' :'Rosacea on the chin'} , 'valueVI' : {'0' :'Không có da viêm mẫn đỏ ở cằm' , '1' :' Xuất hiện da viêm mẫn đỏ ở cằm'} },
// }
// K8 - Pore Detection - Special
const poreArr = [
  "PoresCheeks_Left",
  "PoresCheeks_Right",
  "PoresBetweenBrow_Have",
  "PoresForehead_Have",
];
const poreTrans = {
  title: { en: "Pore Detection", vi: "Nhận diện các vấn đề do lỗ chân lông" },
  descript: {
    en: "Big pores are hard to detect, unless they are seen up close. Our high-resolution pore detection technology helps locate facial pores and address problems before they become noticeable .",
    vi: "Lỗ chân lông to rất khó phát hiện, trừ khi nhìn cận cảnh. Vì vậy, công nghệ phát hiện lỗ chân lông to có độ phân giải cao của chúng tôi sẽ giúp xác định các lỗ chân lông trên khuôn mặt và giải quyết các vấn đề trước khi chúng trở nên đáng báo động.",
  },
  PoresCheeks_Left: {
    en: "Pores Cheeks Left",
    vi: "Lỗ chân lông to ở má trái",
    value: "range",
    valueEN: { 0: "No", 1: "Yes" },
    valueVI: { 0: "Không ", 1: " Có" },
  },
  PoresCheeks_Right: {
    en: "Pores Cheeks Right",
    vi: "Lỗ chân lông to ở má phải",
    value: "range",
    valueEN: { 0: "No", 1: "Yes" },
    valueVI: { 0: "Không ", 1: " Có" },
  },
  PoresBetweenBrow_Have: {
    en: "Pores Between Brow Have",
    vi: "Lỗ chân lông to giữa lông mày",
    value: "range",
    valueEN: { 0: "No", 1: "Yes" },
    valueVI: { 0: "Không ", 1: " Có" },
  },
  PoresForehead_Have: {
    en: "Pores Forehead Have",
    vi: "Lỗ chân lông to trên trán",
    value: "range",
    valueEN: { 0: "No", 1: "Yes" },
    valueVI: { 0: "Không ", 1: " Có" },
  },
};
// K9 - SkinSpot Detection- Special
const skinSpotArr = ["SkinSpot", "SkinAcne"];
const skinSpotTrans = {
  title: {
    en: "SkinSpot And Acne Scar Detection",
    vi: "Nhận diện Đốm thâm/nám và sẹo mụn",
  },
  descript: {
    en: "Spots or dark spots, can be caused by acne scars, excessive sun exposure, or hormonal changes. We can precisely locate each spots on the user's face, thereby recommending appropriate skin treatments.",
    vi: "Các đốm thâm nám có thể bắt nguồn từ việc tiếp xúc với ánh mặt trời, lão hoá, thay đổi hoormon hoặc do các vết thâm mụn để lại. Chúng tôi sẽ nhận diện chính xác vị trí cũng như số lượng các đốm thâm nám trên khuôn mặt của người dùng, từ đó đưa ra các liệu trình chăm sóc phù hợp cho từng trường hợp cụ thể.",
  },
  SkinSpot: {
    en: "Number of SkinSpot",
    vi: " Số lượng Đốm thâm/nám",
    value: "normal",
  },
  SkinAcne: {
    en: "The number of acne",
    vi: "Số lượng sẹo mụn",
    value: "normal",
  },
};

// Conclusion parse data

/**
K1	Skin Age Detection
K2	Skin Type Identification
K3	Skin Tone Detection
K4	Skin Mole  Detection
K5	Signs of Aging Skin Detection
K6	Acne Detection and Skin Rosacea Detection
K7	Dark Circle Detection
K8	Pore Detection
K9	SkinSpot Detection
 *
 * **/
function getDataFromArrByKey(arr, key) {
  return arr.filter((x) => x.key === key);
}

function getDataFromObjByKey(obj, key) {
  return obj.key === key;
}

function processItem(keyword, obj, objAtr) {
  return new Promise((resolve, reject) => {
    const parseObj = objAtr[`${keyword}`];
    try {
      if (parseObj !== null) {
        const result = {};
        let valueEN = "";
        let valueVI = "";
        let drawArr = [];
        if (parseObj.value === "normal") {
          valueEN = parseObj.en + ": " + obj.value;
          valueVI = parseObj.vi + ": " + obj.value;
        } else if (parseObj.value === "range") {
          valueEN = parseObj.en + ": " + parseObj.valueEN[`${obj.value + ""}`];
          valueVI = parseObj.vi + ": " + parseObj.valueVI[`${obj.value + ""}`];
        }
        if (obj.rectangle !== undefined) {
          drawArr = obj.rectangle;
        }
        result.valueEN = valueEN;
        result.valueVI = valueVI;
        result.drawArr = drawArr;
        result.key = keyword;
        result.value = obj.value;

        resolve(result);
      } else {
        resolve(null);
      }
    } catch (e) {
      console.log("eee" + keyword + "  :" + e);
      resolve(null);
    }
  });
}

async function processData(data, typeSDK = "56789") {
  const skinAgeArrResult = {
    title: skinAgeTrans.title,
    descript: skinAgeTrans.descript,
    data: [],
  };
  const skinTypeArrResult = {
    title: skinTypeTrans.title,
    descript: skinTypeTrans.descript,
    data: [],
  };
  const skinToneArrResult = {
    title: skinToneTrans.title,
    descript: skinToneTrans.descript,
    data: [],
  };
  const skinMoleArrResult = {
    title: skinMoleTrans.title,
    descript: skinMoleTrans.descript,
    data: [],
  };
  const agingSkinArrResult = {
    title: agingSkinTrans.title,
    descript: agingSkinTrans.descript,
    data: [],
  };
  const acneArrResult = {
    title: acneTrans.title,
    descript: acneTrans.descript,
    data: [],
  };
  const darkCircleArrResult = {
    title: darkCircleTrans.title,
    descript: darkCircleTrans.descript,
    data: [],
  };
  const poreArrResult = {
    title: poreTrans.title,
    descript: poreTrans.descript,
    data: [],
  };
  const skinSpotArrResult = {
    title: skinSpotTrans.title,
    descript: skinSpotTrans.descript,
    data: [],
  };
  // general Result
  const generalResult = {
    title: { en: "General Result", vi: "Kết quả tổng quan" },
    data: [],
  };
  // Special Result
  const specialResult = {
    title: { en: "Partial Result", vi: "Kết quả từng phần" },
    data: [],
  };
  // general conclusion  type 1,2,3,4
  const generalConclusion = {
    title: { en: "General Conclusion", vi: "Kết luận tổng quan" },
    data: [],
  };
  // Special conclusion type 5,6,7,8,9 ( mỗi loại thì thêm type vô để có cách hiển thị riêng)
  const specialConclusion = {
    title: { en: "Partial Conclusion", vi: "Kết luận từng phần" },
    data: [],
  };

  // init default data for result sdk type
  let totalCountK5 = {
    totalCountK5: 0,
  };
  //   let totalCountK6G1 = 0;
  //   let totalCountK6G2 = 0;
  //   let totalCountK6G3 = 0;
  let totalCountK6 = {
    totalCountK6G1: 0,
    totalCountK6G2: 0,
    totalCountK6G3: 0,
  };
  let totalCountK7 = { totalCountK7: 0 };
  let totalCountK8 = { totalCountK8: 0 };
  let totalCountK9 = { totalCountK9: 0 };

  for (var name in data) {
    if (skinAgeArr.includes(name)) {
      const result1 = await processItem(name, data[`${name}`], skinAgeTrans);
      skinAgeArrResult.data.push(result1);
      getDataFromObjByKey(result1, "SkinAge") &&
        generalConclusion.data.push(result1);
      console.log("1", name);
      continue;
    } else if (skinTypeArr.includes(name)) {
      // console.log(data[`${name}`])
      const result2 = await processItem(name, data[`${name}`], skinTypeTrans);
      // console.log('result', result)
      skinTypeArrResult.data.push(result2);
      getDataFromObjByKey(result2, "SkinType") &&
        generalConclusion.data.push(result2);
      console.log("2", name);
      continue;
    } else if (skinToneArr.includes(name)) {
      const result3 = await processItem(name, data[`${name}`], skinToneTrans);
      skinToneArrResult.data.push(result3);
      getDataFromObjByKey(result3, "SkinLevel") &&
        generalConclusion.data.push(result3);
      console.log("3", name);
      continue;
    } else if (skinMoleArr.includes(name)) {
      const result4 = await processItem(name, data[`${name}`], skinMoleTrans);
      skinMoleArrResult.data.push(result4);
      getDataFromObjByKey(result4, "SkinMole") &&
        generalConclusion.data.push(result4);
      console.log("4", name);
      continue;
    } else if (agingSkinArr.includes(name)) {
      const result5 = await processItem(name, data[`${name}`], agingSkinTrans);
      totalCountK5 = processValueProductK5(
        name,
        parseFloat(data[`${name}`].value),
        totalCountK5.totalCountK5
      );
      agingSkinArrResult.data.push(result5);
      console.log("5", name);
      continue;
    } else if (acneArr.includes(name)) {
      const result6 = await processItem(name, data[`${name}`], acneTrans);
      acneArrResult.data.push(result6);
      console.log("6", name, data[`${name}`].value);
      totalCountK6 = processValueProductK6(
        name,
        parseFloat(data[`${name}`].value),
        totalCountK6.totalCountK6G1,
        totalCountK6.totalCountK6G2,
        totalCountK6.totalCountK6G3
      );
      if (name === "SkinAcne") {
        // special case because skin acne use in k6, k9
        skinSpotArrResult.data.push(result6);
      }
      continue;
    } else if (darkCircleArr.includes(name)) {
      const result7 = await processItem(name, data[`${name}`], darkCircleTrans);
      totalCountK7 = processValueProductK7(
        name,
        parseFloat(data[`${name}`].value),
        totalCountK7.totalCountK7
      );
      darkCircleArrResult.data.push(result7);
      console.log("7", name);
      continue;
    } else if (poreArr.includes(name)) {
      const result8 = await processItem(name, data[`${name}`], poreTrans);
      totalCountK8 = processValueProductK8(
        name,
        parseFloat(data[`${name}`].value),
        totalCountK8.totalCountK8
      );
      poreArrResult.data.push(result8);
      console.log("8", name);
      continue;
    } else if (skinSpotArr.includes(name)) {
      const result10 = await processItem(name, data[`${name}`], skinSpotTrans);
      skinSpotArrResult.data.push(result10);
      totalCountK9 = processValueProductK9(
        name,
        parseFloat(data[`${name}`].value),
        totalCountK9.totalCountK9
      );
      console.log("9", name);
      continue;
    }
  }
  //   console.log('--------------')
  //   console.log('skinAgeArrResult',skinAgeArrResult)
  //   console.log('skinTypeArrResult',skinTypeArrResult)
  //   console.log('skinToneArrResult',skinToneArrResult)
  //   console.log('skinMoleArrResult',skinMoleArrResult)
  //   console.log('agingSkinArrResult',agingSkinArrResult)
  //   console.log('acneArrResult',acneArrResult)
  //   console.log('darkCircleArrResult',darkCircleArrResult)
  //   console.log('poreArrResult',poreArrResult)
  //   console.log('skinSpotArrResult',skinSpotArrResult)
  //   console.log('-------------')
  // PushData general type
  generalResult.data.push(skinAgeArrResult);
  generalResult.data.push(skinTypeArrResult);
  generalResult.data.push(skinToneArrResult);
  generalResult.data.push(skinMoleArrResult);
  let hintResultArr = [
    { sdktype: "K1", level: 1 },
    { sdktype: "K2", level: 1 },
    { sdktype: "K3", level: 1 },
    { sdktype: "K4", level: 1 },
  ];

  if (typeSDK.includes("5")) {
    specialResult.data.push(agingSkinArrResult);
    // console.log('agingSkinArrResult',agingSkinArrResult)
    const result5 = processAginSkinResult(agingSkinArrResult.data);
    if (result5 !== null) {
      specialConclusion.data.push(result5);
    }
    const resultProductK5 = getValueTypeProductK5(totalCountK5.totalCountK5);
    if (resultProductK5 !== null) {
      hintResultArr.push(resultProductK5);
    }
  }
  if (typeSDK.includes("6")) {
    specialResult.data.push(acneArrResult);
    // console.log('acneArrResult',acneArrResult)
    const result6 = processAcneResult(acneArrResult.data);
    if (result6 !== null) {
      specialConclusion.data.push(result6);
    }
    const resultProductK6 = getValueTypeProductK6(
      totalCountK6.totalCountK6G1,
      totalCountK6.totalCountK6G2,
      totalCountK6.totalCountK6G3
    );
    if (resultProductK6 !== null) {
      hintResultArr.push(resultProductK6);
    }
  }
  if (typeSDK.includes("7")) {
    specialResult.data.push(darkCircleArrResult);
    // console.log('darkCircleArrResult',darkCircleArrResult)
    const result7 = processDarkCircleResult(darkCircleArrResult.data);
    // console.log('result7',result7)
    if (result7 !== null) {
      specialConclusion.data.push(result7);
    }
    const resultProductK7 = getValueTypeProductK7(totalCountK7.totalCountK7);
    if (resultProductK7 !== null) {
      hintResultArr.push(resultProductK7);
    }
  }
  if (typeSDK.includes("8")) {
    specialResult.data.push(poreArrResult);
    // console.log('poreArrResult',poreArrResult)
    const result8 = processPoreResult(poreArrResult.data);
    // console.log('result8',result8)
    if (result8 !== null) {
      specialConclusion.data.push(result8);
    }
    const resultProductK8 = getValueTypeProductK8(totalCountK8.totalCountK8);
    if (resultProductK8 !== null) {
      hintResultArr.push(resultProductK8);
    }
  }
  if (typeSDK.includes("9")) {
    specialResult.data.push(skinSpotArrResult);
    // console.log('skinSpotArrResult',skinSpotArrResult)
    const result9 = processSkinSpotResult(skinSpotArrResult.data);
    // console.log('result9',result9)
    if (result9 !== null) {
      specialConclusion.data.push(result9);
    }
    const resultProductK9 = getValueTypeProductK9(totalCountK9.totalCountK9);
    if (resultProductK9 !== null) {
      hintResultArr.push(resultProductK9);
    }
  }
  const finalData = {
    generalResult: generalResult,
    specialResult: specialResult,
    generalConclusion: generalConclusion,
    specialConclusion: specialConclusion,
    hintResult: hintResultArr,
  };
  //   console.log('specialConclusion',JSON.stringify(specialConclusion))
  // console.log("finalData", JSON.stringify(finalData));
  return finalData;
}

// processData(data)

// Process K5- Signs of Aging Skin Detection
const evaluationK5 = [
  "SkinForeHeadWrinkleArea",
  "NasolabialFolds_LeftArea",
  "NasolabialFolds_RightArea",
  "EyeWrinkle_LeftArea",
  "EyeWrinkle_RightArea",
  "CrowsFeed_LeftArea",
  "CrowsFeed_RightArea",
];

function processValueProductK5(name, value, totalCount = 0) {
  if (evaluationK5.includes(name)) {
    totalCount += value;
  }
  return { totalCountK5: totalCount };
}
function getValueTypeProductK5(inputValue) {
  const value = parseFloat(inputValue);
  let valueLevel = 1;
  if (value < 0.001) {
    valueLevel = 1;
  } else if (value <= 0.002) {
    valueLevel = 2;
  } else if (value > 0.002) {
    valueLevel = 3;
  }
  return { level: "K5", sdktype: valueLevel.toString() };
}

// Process K6	 Acne Detection and Skin Rosacea Detection
const evaluationK6G1 = ["SkinBlackHeads", "SkinPimple", "SkinAcne"];
const evaluationK6G2 = ["BlackHead_Area"];
const evaluationK6G3 = [
  "SkinRosaceaNose",
  "SkinRosaceaLeftcheek",
  "SkinRosaceaRightcheek",
  "SkinRosaceaForehead",
  "SkinRosaceaChin",
];
function processValueProductK6(
  name,
  value,
  totalCountG1 = 0,
  totalCountG2 = 0,
  totalCountG3 = 0
) {
  if (evaluationK6G1.includes(name)) {
    totalCountG1 += value;
    // console.log("totalCountG1", totalCountG1);
  } else if (evaluationK6G2.includes(name)) {
    totalCountG2 += value;
    // console.log("totalCountG2", totalCountG2);
  } else if (evaluationK6G3.includes(name)) {
    // console.log("value", typeof value, (totalCountG3 += value));
    totalCountG3 += value;
    // console.log("totalCountG3", totalCountG3);
  }
  return {
    totalCountK6G1: totalCountG1,
    totalCountK6G2: totalCountG2,
    totalCountK6G3: totalCountG3,
  };
}

function getValueTypeProductK6(inputValueG1, inputValueG2, inputValueG3) {
  //   console.log("Input", inputValueG1, inputValueG2, inputValueG3);
  const valueG1 = parseFloat(inputValueG1);
  const valueG2 = parseFloat(inputValueG2);
  const valueG3 = parseFloat(inputValueG3);
  //   console.log("getValueTypeProductK6", valueG1, valueG2, valueG3);
  let valueLevel = 1;
  if (valueG1 > 50 || valueG2 > 0.05 || valueG3 > 4) {
    valueLevel = 6;
  } else if (valueG1 >= 40 || valueG2 >= 0.04 || valueG3 >= 3) {
    valueLevel = 5;
  } else if (valueG1 >= 30 || valueG2 >= 0.03 || valueG3 >= 2) {
    valueLevel = 4;
  } else if (valueG1 >= 20 || valueG2 >= 0.01 || valueG3 >= 1) {
    valueLevel = 3;
  } else if (valueG1 >= 10 || valueG2 >= 0.005) {
    valueLevel = 2;
  } else if (valueG1 < 10 || valueG2 < 0.005) {
    valueLevel = 1;
  }
  return { level: "K6", sdktype: valueLevel.toString() };
}

// K7	Dark Circle Detection
const evaluationK7 = [
  "SkinPandaEye_Left",
  "SkinPandaEye_Right",
  "SkinPandaEye_Left_Pigment",
  "SkinPandaEye_Right_Pigment",
  "SkinPandaEye_Left_Artery",
  "SkinPandaEye_Right_Artery",
  "SkinPandaEye_Left_Shadow",
  "SkinPandaEye_Right_Shadow",
];
function processValueProductK7(name, value, totalCount = 0) {
  if (evaluationK7.includes(name)) {
    totalCount += value;
  }
  return { totalCountK7: totalCount };
}
function getValueTypeProductK7(inputValue) {
  const value = parseFloat(inputValue);
  let valueLevel = 1;
  if (value < 1) {
    valueLevel = 1;
  } else if (value <= 2) {
    valueLevel = 2;
  } else if (value > 2) {
    valueLevel = 3;
  }
  return { level: "K7", sdktype: valueLevel.toString() };
}

// K8	Pore Detection
const evaluationK8 = [
  "PoresCheeks_Left",
  "PoresCheeks_Right",
  "PoresBetweenBrow_Have",
  "PoresForehead_Have",
];
function processValueProductK8(name, value, totalCount = 0) {
  if (evaluationK8.includes(name)) {
    totalCount += value;
  }
  return { totalCountK8: totalCount };
}
function getValueTypeProductK8(inputValue) {
  const value = parseFloat(inputValue);
  let valueLevel = 1;
  if (value < 1) {
    valueLevel = 1;
  } else if (value <= 2) {
    valueLevel = 2;
  } else if (value > 2) {
    valueLevel = 3;
  }
  return { level: "K8", sdktype: valueLevel.toString() };
}

// K9	SkinSpot Detection
const evaluationK9 = ["SkinSpot", "SkinAcne"];
function processValueProductK9(name, value, totalCount = 0) {
  if (evaluationK9.includes(name)) {
    totalCount += value;
  }
  return { totalCountK9: totalCount };
}
function getValueTypeProductK9(inputValue) {
  const value = parseFloat(inputValue);
  let valueLevel = 1;
  if (value < 20) {
    valueLevel = 1;
  } else if (value <= 50) {
    valueLevel = 2;
  } else if (value > 50) {
    valueLevel = 3;
  }
  return { level: "K9", sdktype: valueLevel.toString() };
}

// processData(data);
module.exports = { processData };
//
