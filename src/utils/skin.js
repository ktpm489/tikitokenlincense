//  K1, K2, K3, K4 General
// K1 - Skin Age Detection
const skinAgeArr = ["SkinAge"];
const skinAgeTrans = {
  title: { en: "Skin Age Detection", vi: "Nhận diện tuổi da" },
  descript: {
    en: "Skin age does not necessarily reveal the user’s actual age, but is a great indicator of their health and lifestyle. If the skin age is older than the actual age, it is a sign that users should begin to change their daily routine and skin care.",
    vi: "Tuổi da không nhất thiết tiết lộ tuổi thực của người dùng, nhưng là một chỉ số tuyệt vời về sức khỏe và lối sống của họ. Nếu tuổi da lớn hơn tuổi thực, đó là dấu hiệu để người dùng bắt đầu thay đổi các thói quen sinh hoạt và chăm sóc da của họ.",
  },
  SkinAge: { en: "Skin Age", vi: "Tuổi da", value: "normal" },
};
// K2 - Skin Type Identification
// const skinTypeArr = ["SkinType", "SkinHighlight", "SkinHighlightPrec"];
const skinTypeArr = ["SkinType", "SkinHighlight"];
const skinTypeTrans = {
  title: { en: "Skin Type Identification", vi: "Nhận diện các loại da" },
  descript: {
    en: "Having a beautiful skin begins with knowing your skin type. Scientifically speaking, there are four skin types: oily, dry, normal, and a combination of these three. We identify the skin type found on different areas of the face as well as specific problems that may arise.",
    vi: "Nhận biết chính xác loại da của mình là bước đầu tiên trong việc chăm sóc da. Có bốn loại da cơ bản sau đây: da dầu, da khô, da thường và da hỗn hợp là sự kết hợp của ba loại da này. Chúng tôi xác định loại da được tìm thấy trên các vùng khác nhau của khuôn mặt cũng như các vấn đề cụ thể có thể phát sinh.",
  },
  SkinType: {
    en: "Skin Type",
    vi: "Loại da",
    value: "range",
    valueEN: {
      0: "Oily",
      1: "Dry",
      2: "Normal",
      3: "Combination",
    },
    valueVI: {
      0: "Da dầu",
      1: "Da khô",
      2: "Da trung tính",
      3: "Da hỗn hợp",
    },
  },
  SkinHighlight: {
    en: "The number of oily highlight area",
    vi: "Vùng da dầu",
    value: "normal",
  },
  SkinHighlightPrec: {
    en: "The percentage of oily highlight area (from 0 to 1)",
    vi: "Diện tích da dầu",
    value: "parsenormal",
  },
};
// K3- Skin Tone Detection
const skinToneArr = ["SkinColorHueDelta", "SkinColorLevel", "SkinLevel"];
const skinToneTrans = {
  title: { en: "Skin Tone Detection", vi: "Nhận diện tông màu da" },
  descript: {
    en: "According to international standards, there are 40 different levels of skin tone: our technology can precisely detect the level of each individual user. Morever, we also provide the Pantone SkinTone color, which will help users to easily match and coordinate cosmetics to skin color.",
    vi: "Theo tiêu chuẩn quốc tế, có 40 mức độ khác nhau của tông màu da: công nghệ của chúng tôi có thể phát hiện chính xác tông màu da của mỗi người dùng. Ngoài ra, chúng tôi còn cung cấp thông tin về Mức độ tông màu da theo bảng màu Pantone 110, giúp người dùng có thể dễ dàng hơn trong việc tìm ra những màu sắc phù hợp nhất với làn da của mình.",
  },
  SkinColorHueDelta: {
    en: "Skin Color Level",
    vi: "Màu sắc của da",
    value: "range",
    valueEN: {
      0: "neutral",
      1: "yellow",
      2: "reddish",
    },
    valueVI: {
      0: "Trung tính",
      1: "Vàng",
      2: "Ửng Đỏ",
    },
  },
  SkinLevel: {
    en: "Level of skin tone according to Pantone",
    vi: "Mức độ bản màu quốc tế",
    value: "normal",
  },
  SkinColorLevel: {
    en: "Level of skin tone brightness",
    vi: "Độ sáng màu da",
    value: "range",
    valueEN: {
      1: "Light White",
      2: "White",
      3: "Natural",
      4: "Tan",
      5: "Dark",
      6: "Dull",
    },
    valueVI: {
      1: "Rất Trắng",
      2: "Trắng",
      3: "Tự Nhiên",
      4: "Vàng",
      5: "Ngăm Đen",
      6: "Nhiều Màu",
    },
  },
};
// K4- Skin Mole  Detection
const skinMoleArr = ["SkinMole"];
const skinMoleTrans = {
  title: { en: "Skin Mole Detection", vi: "Nhận diện nốt ruồi" },
  SkinMole: { en: "Skin Mole", vi: "Nốt Ruồi", value: "normal" },
  descript: {
    en: "Skin moles are common, almost everyone has them. Knowing the difference between moles and other spots is vital for knowing which to pay more attention to.",
    vi: "Nốt ruồi trên da là một tình trạng phổ biến mà hầu hết mọi người đều có. Phân biệt sự khác nhau giữa nốt ruồi và các vết nám và thâm khác trên da là điều cần thiết để có sự quan tâm đúng đắn cho từng loại.",
  },
};

// K5 - Signs of Aging Skin Detection  -Special
const agingSkinArr = [
  "SkinForeHeadWrinkle",
  "SkinForeHeadWrinkleArea",
  "SkinEyeFineLine_Left",
  "SkinEyeFineLine_Right",
  "SkinEyeFineLineScore_Left",
  "SkinEyeFineLineScore_Right",
  "SkinCrowsFeed_Left",
  "SkinCrowsFeed_Right",
  "SkinNasolabialFolds_Left",
  "SkinNasolabialFolds_Right",
  "NasolabialFolds_LeftArea",
  "NasolabialFolds_RightArea",
  "SkinEyeWrinkle_Left",
  "SkinEyeWrinkle_Right",
  // "EyeWrinkle_LeftArea",
  // "EyeWrinkle_RightArea",
  "CrowsFeed_LeftArea",
  "CrowsFeed_RightArea",
];
const agingSkinTrans = {
  title: {
    en: "Signs of Aging Skin Detection",
    vi: "Nhận diện các dấu hiệu Lão Hóa Da",
  },
  descript: {
    en: "After identifying different types of facial wrinkles including forehead wrinkles, crow's feet, eye wrinkles, smile lines, and fine lines around the eye, we are able to show users the level of impact and provide more reliable data according to their specific situation.",
    vi: "Sau khi xác định các loại nếp nhăn khác nhau trên khuôn mặt bao gồm nếp nhăn trán, vết chân chim, nếp nhăn mắt, đường cười và nếp nhăn quanh mắt, chúng tôi có thể cho người dùng thấy mức độ tác động và cung cấp dữ liệu đáng tin cậy hơn tùy theo tình hình cụ thể của họ.",
  },
  SkinForeHeadWrinkle: {
    en: "Skin ForeHead Wrinkle",
    vi: "Nhăn trên trán",
    value: "range",
    valueEN: { 0: "No", 1: "Yes" },
    valueVI: { 0: "Không ", 1: " Có" },
  },
  SkinForeHeadWrinkleArea: {
    en: "Skin ForeHead Wrinkle Area",
    vi: "Tỷ lệ nhăn trên trán",
    value: "normal",
  },
  SkinEyeFineLine_Left: {
    en: "Skin Eye Fine Line Left",
    vi: "Nhăn trên mắt trái",
    value: "range",
    valueEN: { 0: "No", 1: "Yes" },
    valueVI: { 0: "Không ", 1: " Có" },
  },
  SkinEyeFineLine_Right: {
    en: "Skin Eye Fine Line Right",
    vi: "Nhăn trên mắt phải",
    value: "range",
    valueEN: { 0: "No", 1: "Yes" },
    valueVI: { 0: "Không ", 1: " Có" },
  },
  SkinEyeFineLineScore_Left: {
    en: "Skin Eye Fine Line Score Left (0 -> 1)",
    vi: "Kết quả phân tích nhăn đuôi mắt trái",
    value: "parsenormal",
  },
  SkinEyeFineLineScore_Right: {
    en: "Skin Eye Fine Line Score Right (0 -> 1)",
    vi: "Kết quả phân tích nhăn đuôi mắt phải",
    value: "parsenormal",
  },
  SkinCrowsFeed_Left: {
    en: "Skin Crows Feed Left",
    vi: "Nhăn đuôi mắt trái",
    value: "range",
    valueEN: { 0: "No", 1: "Yes" },
    valueVI: { 0: "Không ", 1: " Có" },
  },
  SkinCrowsFeed_Right: {
    en: "Skin Crows Feed Right",
    vi: "Nhăn đuôi mắt phải",
    value: "range",
    valueEN: { 0: "No", 1: "Yes" },
    valueVI: { 0: "Không ", 1: " Có" },
  },
  SkinNasolabialFolds_Left: {
    en: "Skin Nasolabial Folds Left",
    vi: "Nhăn cánh mũi trái",
    value: "range",
    valueEN: { 0: "No", 1: "Yes" },
    valueVI: { 0: "Không ", 1: " Có" },
  },
  SkinNasolabialFolds_Right: {
    en: "Skin Nasolabial Folds Right",
    vi: "Nhăn cách mũi phải",
    value: "range",
    valueEN: { 0: "No", 1: "Yes" },
    valueVI: { 0: "Không ", 1: " Có" },
  },
  NasolabialFolds_LeftArea: {
    en: "Nasolabial Folds Left Area (0 -> 1)",
    vi: "Kết quả phân tích nhăn cánh mũi trái",
    value: "parsenormal",
  },
  NasolabialFolds_RightArea: {
    en: "Nasolabial Folds Right Area (0 -> 1)",
    vi: "Kết quả phân tích nhăn cách mũi phải",
    value: "parsenormal",
  },
  SkinEyeWrinkle_Left: {
    en: "Skin Eye Wrinkle Left",
    vi: "Nhăn dưới mắt trái",
    value: "range",
    valueEN: { 0: "No", 1: "Yes" },
    valueVI: { 0: "Không ", 1: " Có" },
  },
  SkinEyeWrinkle_Right: {
    en: "Skin Eye Wrinkle Left",
    vi: "Nhăn dưới mắt phải",
    value: "range",
    valueEN: { 0: "No", 1: "Yes" },
    valueVI: { 0: "Không ", 1: " Có" },
  },
  // EyeWrinkle_LeftArea: {
  //   en: "Eye Wrinkle Left Area (0 -> 1)",
  //   vi: "Tỷ lệ vùng - Da mắt trái nhăn",
  //   value: "parsenormal",
  // },
  // EyeWrinkle_RightArea: {
  //   en: "Eye Wrinkle Right Area (0 -> 1)",
  //   vi: "Tỷ lệ vùng - Da mắt phải nhăn",
  //   value: "parsenormal",
  // },
  CrowsFeed_LeftArea: {
    en: "CrowsFeed Left Area (0 -> 1)",
    vi: "Tỷ lệ diện tích nhăn đuôi mắt trái",
    value: "parsenormal",
  },
  CrowsFeed_RightArea: {
    en: "CrowsFeed Right Area (0 -> 1)",
    vi: "Tỷ lệ diện tích nhăn đuôi mắt phải",
    value: "parsenormal",
  },
};
// K6 - Blackhead And Skin Rosacea Detection - Special
const acneArr = [
  "SkinBlackHeads",
  "BlackHead_Area",
  "SkinPimple",
  "SkinAcne",
  "SkinRosaceaNose",
  "SkinRosaceaLeftcheek",
  "SkinRosaceaRightcheek",
  "SkinRosaceaForehead",
  "SkinRosaceaChin",
];
const acneTrans = {
  title: {
    en: "Acne And Skin Rosacea Detection",
    vi: "Nhận diện mụn và mụn viêm đỏ",
  },
  descript: {
    en: "We can accurately identify symptoms of acne such as pimples, blackheads and acne vulgaris from mild to severe. Especially, with blackheads that would not normally be seen without some kind of magnification, our high-resolution blackhead detection technology can help locate even the most minute blackheads to ensure they have nowhere to hide.Rosacea is a long-term skin condition that causes redness and visible blood vessels in your face.And we can early detection of rosacea can help user to get an appropriate treatment before redness and swelling can get worse and might become permanent.",
    vi: "Chúng tôi sẽ nhận diện được chính xác các triệu chứng của mụn, bao gồm mụn dầu đen, các nốt mụn và do mụn ở mức độ từ nhẹ đến nặng. Đặc biệt, với các nốt mụn đầu đen mà thông thường sẽ không thể nhìn thấy nếu không có một số loại phóng đại , công nghệ phát hiện mụn đầu đen có độ phân giải cao của chúng tôi sẽ giúp nhận diện được vị trí ngay cả những mụn đầu đen nhỏ nhất để đảm bảo chúng không có nơi nào để ẩn náu.Và Tình trạng Mụn viêm đỏ là một tổn thương da lâu dài gây ra sự mẫn đỏ và khiến các mạch máu nổi rõ trên khuôn mặt. Nhận diện sớm tình trạng Mụn viêm đỏ có thể giúp người dùng có những biện pháp điều trị kịp thời trước khi tình trạng chuyển biến nghiêm trọng và có thể trở thành vĩnh viễn.",
  },
  SkinBlackHeads: {
    en: "The number of blackheads",
    vi: "Số lượng mụn đầu đen",
    value: "normal",
  },
  BlackHead_Area: {
    en: "The proportion of the area occupied by blackheads (0->1)",
    vi: "Tỷ lệ vùng da mặt bị mụn đầu đen (0->1)",
    value: "parsenormal",
  },
  SkinPimple: {
    en: "The number of acne scar",
    vi: "Số lượng do mụn",
    value: "normal",
  },
  SkinAcne: {
    en: "The number of acne",
    vi: "Số lượng mụn",
    value: "normal",
  },
  SkinRosaceaNose: {
    en: "Skin Rosacea Nose ",
    vi: "Mụn viêm đỏ - Quanh mũi",
    value: "range",
    valueEN: { 0: 'No rosacea around the nose"', 1: "Rosacea around the nose" },
    valueVI: {
      0: "Không",
      1: "Có",
    },
  },
  SkinRosaceaLeftcheek: {
    en: "Skin Rosacea Left Cheek",
    vi: "Mụn viêm đỏ - Má trái",
    value: "range",
    valueEN: {
      0: "No rosacea on the left cheek",
      1: "Rosacea on the left cheek",
    },
    valueVI: {
      0: "Không",
      1: "Có",
    },
  },
  SkinRosaceaRightcheek: {
    en: "Skin Rosacea Right Cheek",
    vi: "Mụn viêm đỏ - Má phải",
    value: "range",
    valueEN: {
      0: "No rosacea on the right cheek",
      1: "Rosacea on the right cheek",
    },
    valueVI: {
      0: "Không",
      1: "Có",
    },
  },
  SkinRosaceaForehead: {
    en: "Skin Rosacea Forehead",
    vi: "Mụn viêm đỏ - Trán",
    value: "range",
    valueEN: { 0: "No rosacea on the forehead", 1: "Rosacea on the forehead" },
    valueVI: {
      0: "Không",
      1: "Có",
    },
  },
  SkinRosaceaChin: {
    en: "Skin Rosacea Chin",
    vi: "Mụn viêm đỏ - Cằm",
    value: "range",
    valueEN: { 0: "No rosacea on the chin", 1: "Rosacea on the chin" },
    valueVI: {
      0: "Không",
      1: "Có",
    },
  },
};
// K7 - Dark Circle Detection - Special
const darkCircleArr = [
  "SkinPandaEye_Left",
  "SkinPandaEye_Right",
  "SkinPandaEye_Left_Pigment",
  "SkinPandaEye_Right_Pigment",
  "SkinPandaEye_Left_Artery",
  "SkinPandaEye_Right_Artery",
  "SkinPandaEye_Left_Shadow",
  "SkinPandaEye_Right_Shadow",
];
const darkCircleTrans = {
  title: {
    en: "Dark Circle Detection",
    vi: "Nhận diện Quầng thâm mắt",
  },
  descript: {
    en: "In fact, the skin that surrounds your eyes is thinner and more delicate than that on the rest of your face, that's why  dark circles and puffines are more likely to occur. We can scientifically identify the type of dark circles (pigmented or vascular) as well as their level of impact.",
    vi: "Trên thực tế, vùng da xung quanh mắt mỏng hơn vùng còn lại của da mặt, điều này khiến các tình trạng như quầng thâm và bọng mắt dễ xảy ra hơn. Chúng tôi sẽ nhận diện một cách khoa học các vấn đề về quầng thâm mà bạn đang gặp phải (do sắc tố hay do mạch máu) cũng như xác định mức độ nghiêm trọng của chúng.",
  },
  SkinPandaEye_Left: {
    en: "Skin PandaEye Left",
    vi: "Quầng thâm - mắt trái",
    value: "range",
    valueEN: { 0: "No", 1: "Yes" },
    valueVI: { 0: "Không ", 1: " Có" },
  },
  SkinPandaEye_Right: {
    en: "Skin PandaEye Right",
    vi: "Quầng thâm - mắt phải",
    value: "range",
    valueEN: { 0: "No", 1: "Yes" },
    valueVI: { 0: "Không ", 1: " Có" },
  },
  SkinPandaEye_Left_Pigment: {
    en: "Skin PandaEye Left Pigment",
    vi: "Quầng thâm do sắc tố - Mắt Trái",
    value: "range",
    valueEN: { 0: "No", 1: "Light", 2: "Medium", 3: "Severe" },
    valueVI: { 0: "Không", 1: "Nhẹ", 2: "Trung bình", 3: "Nặng" },
  },
  SkinPandaEye_Right_Pigment: {
    en: "Skin PandaEye Right Pigment",
    vi: "Quầng thâm do sắc tố - Mắt Phải",
    value: "range",
    valueEN: { 0: "No", 1: "Light", 2: "Medium", 3: "Severe" },
    valueVI: { 0: "Không", 1: "Nhẹ", 2: "Trung bình", 3: "Nặng" },
  },
  SkinPandaEye_Left_Artery: {
    en: "Skin PandaEye Left Artery",
    vi: "Quầng thâm do mạch máu - Mắt trái",
    value: "range",
    valueEN: { 0: "No", 1: "Light", 2: "Medium", 3: "Severe" },
    valueVI: { 0: "Không", 1: "Nhẹ", 2: "Trung bình", 3: "Nặng" },
  },
  SkinPandaEye_Right_Artery: {
    en: "Skin PandaEye Right Artery",
    vi: "Quầng thâm do mạch máu - Mắt phải",
    value: "range",
    valueEN: { 0: "No", 1: "Light", 2: "Medium", 3: "Severe" },
    valueVI: { 0: "Không", 1: "Nhẹ", 2: "Trung bình", 3: "Nặng" },
  },
  SkinPandaEye_Left_Shadow: {
    en: "Skin PandaEye Left Shadow",
    vi: "Quầng thâm do Bọng Mắt Trái",
    value: "range",
    valueEN: { 0: "No", 1: "Light", 2: "Medium", 3: "Severe" },
    valueVI: { 0: "Không", 1: "Nhẹ", 2: "Trung bình", 3: "Nặng" },
  },
  SkinPandaEye_Right_Shadow: {
    en: "Skin PandaEye Right Shadow",
    vi: "Quầng thâm do Bọng Mắt Phải",
    value: "range",
    valueEN: { 0: "No", 1: "Light", 2: "Medium", 3: "Severe" },
    valueVI: { 0: "Không", 1: "Nhẹ", 2: "Trung bình", 3: "Nặng" },
  },
};

// K8 merge with K6 - Skin Rosacea Detection -Special
// const rosaceaArr = ['SkinRosaceaNose', 'SkinRosaceaLeftcheek','SkinRosaceaRightcheek','SkinRosaceaForehead','SkinRosaceaChin']
// const rosaceaTrans = {
//     'title' : {'en' : 'Skin Rosacea Detection' ,'vi' : 'Nhận diện Mụn viêm đỏ'},
//     'descript' : {'en' : 'Rosacea is a long-term skin condition that causes redness and visible blood vessels in your face. Early detection of rosacea can help user to get an appropriate treatment before redness and swelling can get worse and might become permanent.' , 'vi' : 'Tình trạng Mụn viêm đỏ là một tổn thương da lâu dài gây ra sự mẫn đỏ và khiến các mạch máu nổi rõ trên khuôn mặt. Nhận diện sớm tình trạng Mụn viêm đỏ có thể giúp người dùng có những biện pháp điều trị kịp thời trước khi tình trạng chuyển biến nghiêm trọng và có thể trở thành vĩnh viễn.'},
//     'SkinRosaceaNose' : {'en' : 'Skin Rosacea Nose ' ,'vi' : 'Mụn viêm đỏ - Quanh mũi',  'value' :'range', 'valueEN' :{ '0' : 'No rosacea around the nose"', '1' :'Rosacea around the nose'} , 'valueVI' : {'0' :'Không bị mụn viêm đỏ quanh mũi  ' , '1' :'Xuất hiện mụn viêm đỏ quanh mũi'} },
//     'SkinRosaceaLeftcheek' : {'en' : 'Skin Rosacea Left Cheek' ,'vi' : 'Mụn viêm đỏ - Má trái',  'value' :'range', 'valueEN' :{ '0' : 'No rosacea on the left cheek', '1' :'Rosacea on the left cheek'} , 'valueVI' : {'0' :'Không có Mụn viêm đỏ ở má trái' , '1' :'Xuất hiện Mụn viêm đỏ ở má trái'} },
//     'SkinRosaceaRightcheek' : {'en' : 'Skin Rosacea Right Cheek' ,'vi' : 'Mụn viêm đỏ - Má phải',  'value' :'range', 'valueEN' :{ '0' : 'No rosacea on the right cheek', '1' :'Rosacea on the right cheek'} , 'valueVI' : {'0' :'Không có Mụn viêm đỏ ở má phải' , '1' :'Xuất hiện Mụn viêm đỏ ở má phải'} },
//     'SkinRosaceaForehead' : {'en' : 'Skin Rosacea Forehead' ,'vi' : 'Mụn viêm đỏ - Trán',  'value' :'range', 'valueEN' :{ '0' : 'No rosacea on the forehead', '1' :'Rosacea on the forehead'} , 'valueVI' : {'0' :'Không bị Mụn viêm đỏ ở trên trán' , '1' :' Xuất hiện Mụn viêm đỏ trên trán'} },
//     'SkinRosaceaChin' : {'en' : 'Skin Rosacea Chin' ,'vi' : 'Mụn viêm đỏ - Cằm',  'value' :'range', 'valueEN' :{ '0' : 'No rosacea on the chin', '1' :'Rosacea on the chin'} , 'valueVI' : {'0' :'Không có Mụn viêm đỏ ở cằm' , '1' :' Xuất hiện Mụn viêm đỏ ở cằm'} },
// }
// K8 - Pore Detection - Special
const poreArr = [
  "PoresCheeks_Left",
  "PoresCheeks_Right",
  "PoresBetweenBrow_Have",
  "PoresForehead_Have",
];
const poreTrans = {
  title: { en: "Pore Detection", vi: "Nhận diện các vấn đề do lỗ chân lông" },
  descript: {
    en: "Big pores are hard to detect, unless they are seen up close. Our high-resolution pore detection technology helps locate facial pores and address problems before they become noticeable .",
    vi: "Lỗ chân lông to rất khó phát hiện, trừ khi nhìn cận cảnh. Vì vậy, công nghệ phát hiện lỗ chân lông to có độ phân giải cao của chúng tôi sẽ giúp xác định các lỗ chân lông trên khuôn mặt và giải quyết các vấn đề trước khi chúng trở nên đáng báo động.",
  },
  PoresCheeks_Left: {
    en: "Pores Cheeks Left",
    vi: "Lỗ chân lông to ở má trái",
    value: "range",
    valueEN: { 0: "No", 1: "Yes" },
    valueVI: { 0: "Không ", 1: " Có" },
  },
  PoresCheeks_Right: {
    en: "Pores Cheeks Right",
    vi: "Lỗ chân lông to ở má phải",
    value: "range",
    valueEN: { 0: "No", 1: "Yes" },
    valueVI: { 0: "Không ", 1: " Có" },
  },
  PoresBetweenBrow_Have: {
    en: "Pores Between Brow Have",
    vi: "Lỗ chân lông to giữa lông mày",
    value: "range",
    valueEN: { 0: "No", 1: "Yes" },
    valueVI: { 0: "Không ", 1: " Có" },
  },
  PoresForehead_Have: {
    en: "Pores Forehead Have",
    vi: "Lỗ chân lông to trên trán",
    value: "range",
    valueEN: { 0: "No", 1: "Yes" },
    valueVI: { 0: "Không ", 1: " Có" },
  },
};
// K9 - SkinSpot Detection- Special
const skinSpotArr = ["SkinSpot", "SkinAcne"];
const skinSpotTrans = {
  title: {
    en: "SkinSpot And Acne Scar Detection",
    vi: "Nhận diện Đốm thâm nám và do mụn",
  },
  descript: {
    en: "Spots or dark spots, can be caused by acne scars, excessive sun exposure, or hormonal changes. We can precisely locate each spots on the user's face, thereby recommending appropriate skin treatments.",
    vi: "Các đốm thâm nám có thể bắt nguồn từ việc tiếp xúc với ánh mặt trời, lão hoá, thay đổi hoormon hoặc do các vết thâm mụn để lại. Chúng tôi sẽ nhận diện chính xác vị trí cũng như số lượng các đốm thâm nám trên khuôn mặt của người dùng, từ đó đưa ra các liệu trình chăm sóc phù hợp cho từng trường hợp cụ thể.",
  },
  SkinSpot: {
    en: "Number of SkinSpot",
    vi: " Số lượng Đốm thâm nám",
    value: "normal",
  },
  SkinAcne: {
    en: "The number of acne",
    vi: "Số lượng do mụn",
    value: "normal",
  },
};
//// Conclusion
// K5  0 : light , 1 :medium , 2 severe
// level : 0
const skinSpotResultsArr = [
  [
    {
      title: {
        en: "How to fend off the folds (How to fend off the folds)",
        vi: "Cách chống lại các nếp gấp (healthline.com)",
      },
      data: {
        en: [
          "The lines that will eventually etch your face will depend on several factors including genes and lifestyle. ",
          "4 rules to warding off wrinkles:",
          "protect and repair",
          "make healthy lifestyle choices",
          "choose products by skin condition",
          "adjust crease-causing habits",
        ],
        vi: [
          "Các đường nét trên khuôn mặt của bạn sẽ phụ thuộc vào một số yếu tố bao gồm gen và lối sống.",
          "4 quy tắc để ngăn ngừa nếp nhăn",
          "bảo vệ và sửa chữa",
          "lựa chọn lối sống lành mạnh",
          "chọn sản phẩm theo tình trạng da",
          "điều chỉnh các thói quen gây nhăn",
        ],
      },
    },
    {
      title: { en: "1. Protect and repair", vi: "1. Bảo vệ và sửa chữa" },
      data: {
        en: [
          "Usually, Arm yourself with sunscreen of at least SPF 35 or higher.",
          "Generally, Make hats a part of your everyday clothing and sporting gear, and don sunglasses that protect against UV rays.",
          "Generally, We can help combat and even repair daily skin damage by slathering on an antioxidant serum like vitamin C.",
        ],
        vi: [
          "Thường xuyên Trang bị kem chống nắng ít nhất là SPF 35 hoặc cao hơn",
          "Hãy đội mũ trở thành một phần của quần áo và đồ thể thao hàng ngày của bạn, đồng thời đeo kính râm để chống lại tia UV.",
          "Chúng ta có thể giúp chống lại và thậm chí sửa chữa những tổn thương da hàng ngày bằng cách thoa một loại huyết thanh chống oxy hóa như vitamin C.",
        ],
      },
    },
    {
      title: {
        en: "2. Make healthy lifestyle choices, when possible",
        vi: "2. Thực hiện các lựa chọn lối sống lành mạnh, khi có thể",
      },
      data: {
        en: [
          "Usually, Eat a healthy diet",
          "Usually, Reduce sugar intake",
          " Usually Stay hydrated",
          " Generally, Lower alcohol consumption",
          "Generally Don’t smoke",
          "Generally Exercise",
          "Generally Rest up",
          "Generally Reduce stress",
        ],
        vi: [
          "Ăn một chế độ ăn uống lành mạnh thường xuyên ",
          " Giảm lượng đường thường xuyên",
          "Giảm uống rượu thường xuyên",
          "Đừng hút thuốc",
          "Tập thể dục",
          "Nghỉ ngơi",
          "Giảm căng thẳng",
        ],
      },
    },
    {
      title: {
        en: "3. Choose products based on your skin’s condition",
        vi: "3. Chọn sản phẩm dựa trên tình trạng da của bạn",
      },
      data: {
        en: [
          "No sign of wrinkles forming yet? Keep your product arsenal simple, if you like. Rosehip oil can be a multipurpose workhorse in your skin care routine, serving as a moisturizer, brightener, antioxidant, collagen booster, and more.",
          "Don’t forget to moisturize. Products that contain shea butter are a winning wrinkle weapon. SB’s soothing and smoothing properties repair damage from oxidative stress to prevent further creasing. And it softens and smooths existing lines.",
        ],
        vi: [
          "Chưa có dấu hiệu hình thành nếp nhăn? Giữ cho kho sản phẩm của bạn đơn giản, nếu bạn thích. Dầu tầm xuân có thể là một công cụ đa năng trong thói quen chăm sóc da của bạn, đóng vai trò như một loại kem dưỡng ẩm, chất làm sáng, chất chống oxy hóa, tăng cường collagen, v.v.",
          "Đừng quên dưỡng ẩm. Các sản phẩm có chứa bơ hạt mỡ là một vũ khí làm mờ nếp nhăn. Các đặc tính làm dịu và làm mịn của SB sửa chữa các hư hỏng do ứng suất oxy hóa để ngăn ngừa nhàu thêm. Và nó làm mềm và mịn các đường hiện có.",
        ],
      },
    },
    {
      title: {
        en: "4. Adjust crease-causing habits",
        vi: "4. Điều chỉnh thói quen gây nếp nhăn",
      },
      data: {
        en: [
          "Generally, Don’t squish your face into your pillow. ",
          "Generally, Stop resting your chin, cheeks, or forehead in your hands. ",
          "Generally, Avoid rubbing your eyes. ",
          "Generally, Reduce squinting or furrowing your brow. ",
        ],
        vi: [
          "Đừng cúi mặt vào gối.",
          "Ngừng chống tay lên cằm, má hoặc trán.",
          "Tránh dụi mắt.",
          "Giảm nheo mắt hoặc nhíu mày.",
        ],
      },
    },
  ],
  // level 1 medium

  [
    {
      title: {
        en: "How to fend off the folds (How to fend off the folds)",
        vi: "Cách chống lại các nếp gấp (healthline.com)",
      },
      data: {
        en: [
          "The lines that will eventually etch your face will depend on several factors including genes and lifestyle. ",
          "4 rules to warding off wrinkles:",
          "protect and repair",
          "make healthy lifestyle choices",
          "choose products by skin condition",
          "adjust crease-causing habits",
        ],
        vi: [
          "Các đường nét trên khuôn mặt của bạn sẽ phụ thuộc vào một số yếu tố bao gồm gen và lối sống.",
          "4 quy tắc để ngăn ngừa nếp nhăn",
          "bảo vệ và sửa chữa",
          "lựa chọn lối sống lành mạnh",
          "chọn sản phẩm theo tình trạng da",
          "điều chỉnh các thói quen gây nhăn",
        ],
      },
    },
    {
      title: { en: "1. Protect and repair", vi: "1. Bảo vệ và sửa chữa" },
      data: {
        en: [
          "You should always Arm yourself with sunscreen of at least SPF 35 or higher.",
          "Usually, Make hats a part of your everyday clothing and sporting gear, and don sunglasses that protect against UV rays.",
          " Usually, We can help combat and even repair daily skin damage by slathering on an antioxidant serum like vitamin C.",
        ],
        vi: [
          "Luôn luôn Trang bị kem chống nắng ít nhất là SPF 35 hoặc cao hơn",
          "Thường xuyên đội mũ trở thành một phần của quần áo và đồ thể thao hàng ngày của bạn, đồng thời đeo kính râm để chống lại tia UV.",
          "Chúng ta có thể giúp chống lại và thậm chí sửa chữa những tổn thương da hàng ngày bằng cách thoa một loại huyết thanh chống oxy hóa như vitamin C một cách thường xuyên.",
        ],
      },
    },
    {
      title: {
        en: "2. Make healthy lifestyle choices, when possible",
        vi: "2. Thực hiện các lựa chọn lối sống lành mạnh, khi có thể",
      },
      data: {
        en: [
          "You should always Eat a healthy diet",
          "You should always Reduce sugar intake",
          "You should always Stay hydrated",
          " Usually, Lower alcohol consumption",
          " Usually, Don’t smoke",
          " Usually Exercise",
          " Usually Rest up",
          " Usually Reduce stress",
        ],
        vi: [
          "Luôn luôn Ăn một chế độ ăn uống lành mạnh",
          "Luôn luôn Giảm lượng đường",
          "Giảm uống rượu thường xuyên",
          "Đừng hút thuốc thường xuyên",
          "Tập thể dục thường xuyên",
          "Nghỉ ngơi thường xuyên",
          "Giảm căng thẳng thường xuyên",
        ],
      },
    },
    {
      title: {
        en: "3. Choose products based on your skin’s condition",
        vi: "3. Chọn sản phẩm dựa trên tình trạng da của bạn",
      },
      data: {
        en: [
          "Starting to feel a bit dry with age? Tap into the elasticity-boosting and moisturizing action of hyaluronic acid. This will be your bestie, keeping your skin pampered and plump.",
          "Don’t forget to moisturize. Products that contain shea butter are a winning wrinkle weapon. SB’s soothing and smoothing properties repair damage from oxidative stress to prevent further creasing. And it softens and smooths existing lines.",
        ],
        vi: [
          " Bắt đầu cảm thấy một chút khô khan với tuổi tác? Khai thác hoạt động tăng độ đàn hồi và dưỡng ẩm của axit hyaluronic. Đây sẽ là tiền đề của bạn, giữ cho làn da của bạn được nâng niu và căng mọng.",
          "Đừng quên dưỡng ẩm. Các sản phẩm có chứa bơ hạt mỡ là một vũ khí làm mờ nếp nhăn. Các đặc tính làm dịu và làm mịn của SB sửa chữa các hư hỏng do ứng suất oxy hóa để ngăn ngừa nhàu thêm. Và nó làm mềm và mịn các đường hiện có.",
        ],
      },
    },
    {
      title: {
        en: "4. Adjust crease-causing habits",
        vi: "4. Điều chỉnh thói quen gây nếp nhăn",
      },
      data: {
        en: [
          "Generally, Don’t squish your face into your pillow. ",
          "Generally, Stop resting your chin, cheeks, or forehead in your hands. ",
          "Generally, Avoid rubbing your eyes. ",
          "Generally, Reduce squinting or furrowing your brow. ",
        ],
        vi: [
          "Đừng cúi mặt vào gối thường xuyên.",
          "Ngừng chống tay tay lên cằm, má hoặc trán thường xuyên.",
          "Tránh dụi mắt thường xuyên.",
          "Giảm nheo mắt hoặc nhíu mày thường xuyên.",
        ],
      },
    },
  ],
  // level 2

  [
    {
      title: {
        en: "How to fend off the folds (How to fend off the folds)",
        vi: "Cách chống lại các nếp gấp (healthline.com)",
      },
      data: {
        en: [
          "The lines that will eventually etch your face will depend on several factors including genes and lifestyle. ",
          "4 rules to warding off wrinkles:",
          "protect and repair",
          "make healthy lifestyle choices",
          "choose products by skin condition",
          "adjust crease-causing habits",
        ],
        vi: [
          "Các đường nét trên khuôn mặt của bạn sẽ phụ thuộc vào một số yếu tố bao gồm gen và lối sống.",
          "4 quy tắc để ngăn ngừa nếp nhăn",
          "bảo vệ và sửa chữa",
          "lựa chọn lối sống lành mạnh",
          "chọn sản phẩm theo tình trạng da",
          "điều chỉnh các thói quen gây nhăn",
        ],
      },
    },
    {
      title: { en: "1. Protect and repair", vi: "1. Bảo vệ và sửa chữa" },
      data: {
        en: [
          "You should always Arm yourself with sunscreen of at least SPF 35 or higher.",
          "Usually, Make hats a part of your everyday clothing and sporting gear, and don sunglasses that protect against UV rays.",
          " Usually, We can help combat and even repair daily skin damage by slathering on an antioxidant serum like vitamin C.",
        ],
        vi: [
          "Luôn luôn Trang bị kem chống nắng ít nhất là SPF 35 hoặc cao hơn",
          "Thường xuyên đội mũ trở thành một phần của quần áo và đồ thể thao hàng ngày của bạn, đồng thời đeo kính râm để chống lại tia UV.",
          "Chúng ta có thể giúp chống lại và thậm chí sửa chữa những tổn thương da hàng ngày bằng cách thoa một loại huyết thanh chống oxy hóa như vitamin C một cách thường xuyên.",
        ],
      },
    },
    {
      title: {
        en: "2. Make healthy lifestyle choices, when possible",
        vi: "2. Thực hiện các lựa chọn lối sống lành mạnh, khi có thể",
      },
      data: {
        en: [
          "You should always Eat a healthy diet",
          "You should always Reduce sugar intake",
          "You should always Stay hydrated",
          "You should always Lower alcohol consumption",
          "You should always Don’t smoke",
          "You should always do exercise",
          "You should always Rest up as most as possible",
          "You should always reduce stress",
        ],
        vi: [
          "Luôn luôn Ăn một chế độ ăn uống lành mạnh",
          "Luôn luôn Giảm lượng đường",
          "Không uống rượu",
          "Không hút thuốc",
          "Luôn luôn tập thể dục",
          "Nghỉ ngơi nhiều nhất có thể",
          "Luôn luôn giảm căng thẳng đến mức tối đa",
        ],
      },
    },
    {
      title: {
        en: "3. Choose products based on your skin’s condition",
        vi: "3. Chọn sản phẩm dựa trên tình trạng da của bạn",
      },
      data: {
        en: [
          "Feel a sag coming on? Retinoids and vitamin C serums are excellent go-to crease fighters. These bad boys will battle sagging before it begins and reduce fine lines and under-eye circles. Look for a product that pairs these ingredients together.",
          "Don’t forget to moisturize. Products that contain shea butter are a winning wrinkle weapon. SB’s soothing and smoothing properties repair damage from oxidative stress to prevent further creasing. And it softens and smooths existing lines.",
        ],
        vi: [
          "Cảm thấy sắp bị chùng xuống? Retinoids và huyết thanh vitamin C là những chất chống lại nếp nhăn tuyệt vời. Những chàng trai xấu này sẽ chiến đấu với tình trạng chảy xệ trước khi nó bắt đầu và làm giảm nếp nhăn và quầng thâm mắt. Tìm kiếm một sản phẩm kết hợp các thành phần này với nhau.",
          "Đừng quên dưỡng ẩm. Các sản phẩm có chứa bơ hạt mỡ là một vũ khí làm mờ nếp nhăn. Các đặc tính làm dịu và làm mịn của SB sửa chữa các hư hỏng do ứng suất oxy hóa để ngăn ngừa nhàu thêm. Và nó làm mềm và mịn các đường hiện có.",
        ],
      },
    },
    {
      title: {
        en: "4. Adjust crease-causing habits",
        vi: "4. Điều chỉnh thói quen gây nếp nhăn",
      },
      data: {
        en: [
          "You should always Don’t squish your face into your pillow. ",
          "You should always Stop resting your chin, cheeks, or forehead in your hands. ",
          "You should always Avoid rubbing your eyes. ",
          "You should always reduce squinting or furrowing your brow. ",
        ],
        vi: [
          "Không cúi mặt vào gối.",
          "Không chống tay lên cằm, má hoặc trán.",
          "Không dụi mắt.",
          "Không nheo mắt hoặc nhíu mày.",
        ],
      },
    },
  ],
];

function processAginSkinResult(data) {
  // let arrContainer = ['SkinForeHeadWrinkleArea', 'NasolabialFolds_LeftArea', 'NasolabialFolds_RightArea', 'EyeWrinkle_LeftArea', 'EyeWrinkle_RightArea', 'CrowsFeed_LeftArea','CrowsFeed_RightArea']
  let arrContainer = [
    "SkinForeHeadWrinkleArea",
    "NasolabialFolds_LeftArea",
    "NasolabialFolds_RightArea",
    "CrowsFeed_LeftArea",
    "CrowsFeed_RightArea",
  ];
  let sum = 0;
  for (let i = 0; i < data.length; i++) {
    if (arrContainer.includes(data[i].key)) {
      sum = sum + parseFloat(data[i].value);
    }
  }
  // -1 no , 0 ,1, ,2  === light ,medium , severe
  let result = -1;
  if (sum === 0) {
    result = -1;
  } else if (sum < 0.001) {
    result = 0;
  } else if (sum <= 0.002) {
    result = 1;
  } else {
    result = 2;
  }

  if (result !== -1) {
    return skinSpotResultsArr[result];
  } else {
    return null;
  }
}

// K6 Acne Detection

/**
 * 
 * {
  	'title' : {'en' : '' ,'vi' : ''},
  	'data' : {
  			'en' : [] ,'vi' : []
  	}
 }
 */
// level arr
const levelArr = [
  {
    title: { en: "Level 1", vi: "Mức độ 1" },
    data: {
      en: [
        "Sub-clinical acne (home treatment or no treatment), a few closed comedones (whiteheads), and open comedones, (blackheads).",
      ],
      vi: [
        "Mụn trứng cá cận lâm sàng (điều trị tại nhà hoặc không điều trị), một vài mụn trứng cá đóng (mụn đầu trắng) và mụn trứng cá mở, (mụn đầu đen).",
      ],
    },
  },
  {
    title: { en: "Level 2", vi: "Mức độ 2" },
    data: {
      en: [
        "Comedonal acne, areas of papules, that are small, solid red bumps on the skin surface.",
      ],
      vi: [
        "Các vết mụn không viêm, các vùng sẩn, là những mụn đỏ rắn, nhỏ trên bề mặt da.",
      ],
    },
  },
  {
    title: { en: "Level 3", vi: "Mức độ 3" },
    data: {
      en: [
        "Mild acne, when the papules are inflamed with some as pustules, containing pus, which is the result of rapid bacterial expansion.",
      ],
      vi: [
        "Mụn trứng cá nhẹ, khi các mụn sẩn bị viêm với một số như mụn mủ, có chứa mủ, là kết quả của sự mở rộng nhanh chóng của vi khuẩn.",
      ],
    },
  },
  {
    title: { en: "Level 4", vi: "Mức độ 4" },
    data: {
      en: ["Moderate acne,  many inflamed papules & pustules"],
      vi: ["Mức độ mụn trung bình, nhiều mụn sẩn viêm & mụn mủ"],
    },
  },
  {
    title: { en: "Level 5", vi: "Mức độ 5" },
    data: {
      en: [
        "Severe nodular acne, inflamed papules & pustules with several deep nodular lesions, which can cause permanent damage to the dermis – which means acne scars.",
      ],
      vi: [
        "Mụn u nặng, mụn viêm và mụn mủ với một số tổn thương nốt sâu, có thể gây tổn thương vĩnh viễn cho lớp hạ bì - có nghĩa là mụn viêm đỏ.",
      ],
    },
  },
  {
    title: { en: "Level 6", vi: "Mức độ 6" },
    data: {
      en: [
        "Severe cystic acne, or severe back acne, many nodular cystic lesions and scaring, permanent alteration of the dermis.",
      ],
      vi: [
        "Mụn trứng cá dạng nang nặng, tổn thương dạng nang nhiều nốt và đóng vảy, thay đổi vĩnh viễn lớp hạ bì.",
      ],
    },
  },
];
// cause  n14 > 10 ,n16 > 10, n17 > 10
const causeArr = [
  {
    title: { en: "Cause of Blackheads ", vi: "Nguyên nhân Mụn đầu đen" },
    data: {
      en: [
        "Blackheads occur when a pore is clogged by a combination of sebum and dead skin cells. The top of the pore stays open, despite the rest of it being clogged. This results in the characteristic black color seen on the surface.",
      ],
      vi: [
        "Mụn đầu đen xảy ra khi lỗ chân lông bị tắc do sự kết hợp của bã nhờn và tế bào da chết. Phần trên cùng của lỗ chân lông vẫn mở, mặc dù phần còn lại của nó bị tắc nghẽn. Điều này dẫn đến màu đen đặc trưng nhìn thấy trên bề mặt.",
      ],
    },
  },
  {
    title: { en: "Cause of Acne ", vi: "Nguyên nhân mụn viêm" },
    data: {
      en: [
        "Although sebum and dead skin cells contribute to inflammatory acne, bacteria can also play a role in clogging up pores. Bacteria can cause an infection deep beneath the skin’s surface. This may result in painful acne spots that are hard to get rid of.",
      ],
      vi: [
        "Mặc dù bã nhờn và tế bào da chết góp phần gây ra mụn viêm, vi khuẩn cũng có thể đóng một vai trò trong việc làm tắc nghẽn lỗ chân lông. Vi khuẩn có thể gây nhiễm trùng sâu bên dưới bề mặt da. Điều này có thể dẫn đến những nốt mụn sưng tấy và khó loại bỏ.",
      ],
    },
  },
  {
    title: { en: "Cause of Depressed acne scars ", vi: "Nguyên nhân sẹo Lõm" },
    data: {
      en: [
        "If the body produces too little collagen, depressions or pits form as the skin heals.",
      ],
      vi: [
        "Nếu cơ thể sản xuất quá ít collagen, các vết lõm sẽ hình thành khi da lành lại.",
      ],
    },
  },
  {
    title: { en: "Cause of Raised acne scars", vi: "Nguyên nhân sẹo Lồi" },
    data: {
      en: [
        "Sometimes the body produces too much collagen as it tries to heal the skin and underlying tissue. When this happens, a person develops a raised acne scar. This type of acne scar is more common in people who have skin of color like African Americans, Hispanics, and Asians.",
      ],
      vi: [
        "Đôi khi cơ thể sản xuất quá nhiều collagen vì nó cố gắng chữa lành da và mô bên dưới. Khi điều này xảy ra, một người sẽ phát triển một vết mụn viêm đỏ lớn lên. Loại mụn viêm đỏ trứng cá này phổ biến hơn ở những người có màu da như người Mỹ gốc Phi, người Tây Ban Nha và người châu Á.",
      ],
    },
  },
];

function processAcneResult(data) {
  // n14 : SkinBlackHeads, n16 : SkinPimple , n17 : SkinAcne ( gia tri so)
  let arrType1 = ["SkinBlackHeads", "SkinPimple", "SkinAcne"];
  // N15
  let arrType2 = ["BlackHead_Area"];
  // N18, n19,n20,n21,n22
  let arrType3 = [
    "SkinRosaceaChin",
    "SkinRosaceaForehead",
    "SkinRosaceaLeftcheek",
    "SkinRosaceaNose",
    "SkinRosaceaRightcheek",
  ];
  let sumType1 = 0;
  let sumType2 = 0;
  let sumType3 = 0;
  let result = [];
  let n14 = 0;
  let n16 = 0;
  let n17 = 0;
  for (let i = 0; i < data.length; i++) {
    if (arrType1.includes(data[i].key)) {
      sumType1 = sumType1 + parseInt(data[i].value);
      if (data[i].key === "SkinBlackHeads") {
        n14 = parseInt(data[i].value);
      } else if (data[i].key === "SkinPimple") {
        n16 = parseInt(data[i].value);
      } else if (data[i].key === "SkinAcne") {
        n17 = parseInt(data[i].value);
      }
    } else if (arrType2.includes(data[i].key)) {
      sumType2 = sumType2 + parseFloat(data[i].value);
    } else if (arrType3.includes(data[i].key)) {
      sumType3 = sumType1 + parseInt(data[i].value);
    }
  }
  //remove type3
  if (sumType1 > 20 || sumType2 > 0.05) {
    result.push(levelArr[5]);
  } else if (sumType1 >= 15 || sumType2 >= 0.04) {
    result.push(levelArr[4]);
  } else if (sumType1 >= 10 || sumType2 >= 0.03) {
    result.push(levelArr[3]);
  } else if (sumType1 >= 6 || sumType2 >= 0.01) {
    result.push(levelArr[2]);
  } else if (sumType1 >= 3 || sumType2 >= 0.005) {
    result.push(levelArr[1]);
  } else if (
    (sumType1 < 10 && sumType1 > 0) ||
    (sumType2 < 0.005 && sumType2 > 0)
  ) {
    result.push(levelArr[0]);
  }

  if (n14 > 0) {
    result.push(causeArr[0]);
  }
  if (n16 > 0) {
    result.push(causeArr[1]);
  }

  if (n17 > 0) {
    result.push(causeArr[2]);
    result.push(causeArr[3]);
  }

  if (result.length > 0) {
    return result;
  } else {
    return null;
  }
}

// K7 Dark circle detection
const levelK7 = [
  {
    title: { en: "Level of  Dark eye circles", vi: "Mức độ quầng thâm" },
    data: {
      en: ["Light"],
      vi: ["Nhẹ"],
    },
  },
  {
    title: { en: "Level of  Dark eye circles", vi: "Mức độ quầng thâm" },
    data: {
      en: ["Medium"],
      vi: ["Trung bình"],
    },
  },
  {
    title: { en: "Level of  Dark eye circles", vi: "Mức độ quầng thâm" },
    data: {
      en: ["Severe"],
      vi: ["Nặng"],
    },
  },
];

const generalInfoK7 = [
  {
    title: {
      en: "At-Home treatments (healthline.com)",
      vi: "Điều trị tại nhà (healthline.com)",
    },
    data: {
      en: [
        "Apply a cold compress.",
        "Get extra sleep.",
        "Elevate your head..",
        "Soak with tea bags.",
      ],
      vi: [
        "Chườm lạnh.",
        "Ngủ thêm.",
        "Nâng cao đầu của bạn ",
        "Ngâm với túi trà.",
      ],
    },
  },
];

const causeArrK7 = [
  {
    title: { en: "Medications", vi: "Thuốc" },
    data: {
      en: [
        "Any medications that cause blood vessels to dilate can cause circles under the eyes to darken. The skin under the eyes is very delicate, any increased blood flow shows through the skin. ",
      ],
      vi: [
        "Bất kỳ loại thuốc nào làm cho các mạch máu giãn ra có thể làm cho các vùng da vòng tròn dưới mắt bị thâm. Bởi vì vùng da dưới mắt rất mỏng manh, bất kỳ lưu lượng máu tăng lên đều thể hiện qua da.",
      ],
    },
  },
  {
    title: { en: "Anemia", vi: "Thiếu máu" },
    data: {
      en: [
        `The lack of nutrients in the diet, or the lack of a balanced diet, can contribute to the discoloration of the area under the eyes. It is believed that iron deficiency can cause dark circles as well. Iron deficiency is the most common type of anemia and this condition is a sign that not enough oxygen is getting to the body tissues. 
            The skin can also become more pale during pregnancy and menstruation (due to lack of iron), allowing the underlying veins under the eyes to become more visible. `,
      ],
      vi: [
        "Việc thiếu chất dinh dưỡng trong chế độ ăn uống, hoặc thiếu một chế độ ăn uống cân bằng, có thể góp phần vào sự đổi màu của khu vực dưới mắt. Người ta tin rằng thiếu sắt cũng có thể gây ra quầng thâm. Thiếu sắt là loại thiếu máu phổ biến nhất và tình trạng này là dấu hiệu cho thấy không có đủ oxy đến các mô cơ thể.",
      ],
    },
  },
  {
    title: { en: "Fatigue", vi: "Mệt mỏi" },
    data: {
      en: [
        "A lack of sleep and mental fatigue can cause paleness of the skin, allowing the blood underneath the skin to become more visible and appear more blue or darker",
      ],
      vi: [
        "Thiếu ngủ và mệt mỏi về tinh thần có thể gây ra sự tái nhợt của da, cho phép máu bên dưới da trở nên rõ hơn và làm da có màu xanh hoặc tối hơn",
      ],
    },
  },
  {
    title: { en: "Age", vi: "Tuổi tác" },
    data: {
      en: [
        "Dark circles are likely to become more noticeable and permanent with age. This is because as people get older, their skin loses collagen, becoming thinner and more translucent. Circles may also gradually begin to appear darker in one eye than the other as a result of some habitual facial expressions, such as an uneven smile.[citation needed] ",
      ],
      vi: [
        "Quầng thâm có khả năng trở nên đáng chú ý và lâu dài hơn theo tuổi tác. Điều này là do khi mọi người già đi, làn da của họ mất collagen, trở nên mỏng hơn và mờ hơn. Vòng tròn cũng có thể dần dần bắt đầu xuất hiện tối hơn ở một mắt so với mắt kia do một số biểu hiện trên khuôn mặt theo thói quen, chẳng hạn như một nụ cười không cân bằng",
      ],
    },
  },
  {
    title: { en: "Sun exposure", vi: "Phơi nắng" },
    data: {
      en: [
        "Prompts your body to produce more melanin, the pigment that gives skin its color.",
      ],
      vi: [
        "Nắng khiến cơ thể bạn sản xuất nhiều melanin, sắc tố làm sẫm màu da.",
      ],
    },
  },
  {
    title: {
      en: "Periorbital hyperpigmentation",
      vi: "Tăng sắc tố ngoại biên",
    },
    data: {
      en: [
        "Periorbital hyperpigmentation is the official name for when there is more melanin produced around the eyes than is usual, giving them a darker color. ",
      ],
      vi: [
        "Tăng sắc tố ngoại biên là tên chính thức của trạng thái khi có nhiều melanin được sản xuất xung quanh mắt hơn bình thường, khiến quầng mắt có màu tối hơn.",
      ],
    },
  },
];

function processDarkCircleResult(data) {
  /**
   *  N25  Dark eye circles pigment type (left)
   *  N26  Dark eye circles pigment type (right)
   *  N27  Dark eye circles vascular type (left)
   *  N28  Dark eye circles vascular type (right)
   *  N29  Dark eye circles shadow type (left)
   *  N30  Dark eye circles shadow type (right)
   * **/
  let n25 = 0;
  let n26 = 0;
  let n27 = 0;
  let n28 = 0;
  let n29 = 0;
  let n30 = 0;
  let result = [];
  for (let i = 0; i < data.length; i++) {
    if (data[i].key === "SkinPandaEye_Left_Pigment") {
      n25 = parseInt(data[i].value);
    }
    if (data[i].key === "SkinPandaEye_Right_Pigment") {
      n26 = parseInt(data[i].value);
    }
    if (data[i].key === "SkinPandaEye_Left_Artery") {
      n27 = parseInt(data[i].value);
    }
    if (data[i].key === "SkinPandaEye_Right_Artery") {
      n28 = parseInt(data[i].value);
    }
    if (data[i].key === "SkinPandaEye_Left_Shadow") {
      n29 = parseInt(data[i].value);
    }
    if (data[i].key === "SkinPandaEye_Right_Shadow") {
      n30 = parseInt(data[i].value);
    }
  }
  let sum = n25 + n26 + n27 + n28 + n29 + n30;
  if (sum === 0) {
    return null;
  } else {
    if (sum === 1) {
      result.push(levelK7[0]);
    } else if (sum <= 2) {
      result.push(levelK7[1]);
    } else {
      result.push(levelK7[2]);
    }
    result.push(generalInfoK7[0]);

    if (n25 >= 1 || n26 >= 1) {
      result.push(causeArrK7[2]);
      result.push(causeArrK7[4]);
      result.push(causeArrK7[5]);
    }

    if (n27 >= 1 || n28 >= 1) {
      result.push(causeArrK7[0]);
      result.push(causeArrK7[1]);
    }

    if (n27 >= 1 || n28 >= 1) {
      result.push(causeArrK7[3]);
    }
    return result;
  }
}

// k8
const levelk8 = [
  {
    title: { en: "Large Pore Levels", vi: "Mức độ lỗ chân lông to" },
    data: {
      en: ["Light"],
      vi: ["Nhẹ"],
    },
  },
  {
    title: { en: "Large Pore Levels", vi: "Mức độ lỗ chân lông to" },
    data: {
      en: ["Medium"],
      vi: ["Trung bình"],
    },
  },
  {
    title: { en: "Large Pore Levels", vi: "Mức độ lỗ chân lông to" },
    data: {
      en: ["Severe"],
      vi: ["Nặng"],
    },
  },
];

const generalInfoK8 = [
  {
    title: {
      en: "If your pores appear larger, it may be because of: (healthline.com)",
      vi: "Nếu lỗ chân lông của bạn có vẻ to hơn, có thể là do: (healthline.com)",
    },
    data: {
      en: [
        "Acne",
        "Increased sebum production, which causes oily skin",
        "Sun damage",
        "Noncomedogenic Makeup",
      ],
      vi: [
        "Mụn",
        "Tăng sản xuất bã nhờn, gây ra da nhờn",
        "Tác hại của ánh nắng mặt trời",
        "Mỹ phẩm không chứa các thành phần đã được chứng minh là có khả năng gây bít tắc lỗ chân lông",
      ],
    },
  },
];

const causeArrK8 = [
  // light
  [
    {
      title: {
        en: "1. Assess your skin care products",
        vi: "1. Đánh giá các sản phẩm chăm sóc da của bạn",
      },
      data: {
        en: [
          "You should occasionally use Any products designed to clear excess sebum and acne.These products rely on active ingredients such as salicylic acid to remove the top layers of your skin.",
          "To avoid this, you should only generally use the following products for a couple of weeks at a time: Astringents; deep-cleaning facial scrubs; oil-based masks.Also, make sure that all your products are noncomedogenic. ",
        ],
        vi: [
          "Bạn nên  chỉ thỉnh thoảng dùng bất kỳ sản phẩm nào được thiết kế để làm sạch bã nhờn dư thừa và mụn trứng cá dựa vào các thành phần hoạt tính như axit salicylic để loại bỏ các lớp trên cùng của da.",
          "Để tránh điều này, chỉ sử dụng các sản phẩm sau trong vài tuần tại một thời điểm: Chất làm se da; tẩy tế bào chết làm sạch sâu da mặt; mặt nạ dầu. Ngoài ra, hãy đảm bảo rằng tất cả các sản phẩm của bạn đều không gây dị ứng.",
        ],
      },
    },
    {
      title: { en: "2. Cleanse your face", vi: "2. Làm sạch da mặt" },
      data: {
        en: [
          "You should generally use the best types of cleansers get rid of excess dirt and oil without completely stripping your skin of moisture.And avoid cleansers that contain soap or scrubbing agents",
        ],
        vi: [
          "Bạn nên dùng các loại sữa rửa mặt tốt nhất giúp loại bỏ bụi bẩn và dầu thừa mà không làm mất đi độ ẩm hoàn toàn trên da của bạn.Hãy tránh những loại sữa rửa mặt có chứa xà phòng hoặc chất tẩy rửa",
        ],
      },
    },
    {
      title: {
        en: "3. Exfoliate with AHAs or BHAs",
        vi: "3. Tẩy tế bào chết với AHA hoặc BHA",
      },
      data: {
        en: [
          "You should generally follow the American Academy of Dermatology recommends exfoliating just one to two times per week.",
          "If you can, you should always opt for exfoliants with either alpha-hydroxy acids (AHAs) or beta-hydroxy acids (BHAs). ",
        ],
        vi: [
          "Học viện Da liễu Hoa Kỳ khuyến nghị chỉ nên tẩy tế bào chết 1-2 lần mỗi tuần.",
          "Nếu bạn có thể, hãy  luôn luôn chọn sản phẩm tẩy tế bào chết có axit alpha-hydroxy (AHA) hoặc axit beta-hydroxy (BHA).",
        ],
      },
    },
    {
      title: {
        en: "4. You should  generally Moisturize for balanced hydration",
        vi: "4. Dưỡng ẩm để cân bằng độ ẩm",
      },
      data: {
        en: [],
        vi: [],
      },
    },
    {
      title: {
        en: "5. You should usually use a clay mask",
        vi: "5. Bạn nên thường xuyên sử dụng mặt nạ đất sét",
      },
      data: {
        en: [],
        vi: [],
      },
    },
    {
      title: {
        en: "6. You should generally use Wear sunscreen every day",
        vi: "Thoa kem chống nắng mỗi ngày",
      },
      data: {
        en: [
          "Use a product with an SPF of at least 30. You should apply it at least 15 minutes before you head outside.",
        ],
        vi: [
          "Sử dụng sản phẩm có chỉ số SPF ít nhất là 30. Bạn nên thoa sản phẩm ít nhất 15 phút trước khi ra ngoài. ",
        ],
      },
    },
    {
      title: {
        en: "7. You should always don’t sleep with makeup on",
        vi: "7. Bạn không nên ngủ khi trang điểm",
      },
      data: {
        en: [],
        vi: [],
      },
    },
    {
      title: {
        en: "7. You should always stay hydrated",
        vi: "8. Bạn nên luôn luôn giữ đủ nước",
      },
      data: {
        en: [],
        vi: [],
      },
    },
  ],

  // medium

  [
    {
      title: {
        en: "1. Assess your skin care products",
        vi: "1. Đánh giá các sản phẩm chăm sóc da của bạn",
      },
      data: {
        en: [
          "You should Rarely use Any products designed to clear excess sebum and acne.These products rely on active ingredients such as salicylic acid to remove the top layers of your skin.",
          "To avoid this, you should only usually use the following products for a couple of weeks at a time: Astringents; deep-cleaning facial scrubs; oil-based masks.Also, make sure that all your products are noncomedogenic. ",
        ],
        vi: [
          "Bạn nên hiếm khi dùng bất kỳ sản phẩm nào được thiết kế để làm sạch bã nhờn dư thừa và mụn trứng cá dựa vào các thành phần hoạt tính như axit salicylic để loại bỏ các lớp trên cùng của da.",
          "Để tránh điều này, chỉ sử dụng  thường xuyên các sản phẩm sau trong vài tuần tại một thời điểm: Chất làm se da; tẩy tế bào chết làm sạch sâu da mặt; mặt nạ dầu. Ngoài ra, hãy đảm bảo rằng tất cả các sản phẩm của bạn đều không gây dị ứng.",
        ],
      },
    },
    {
      title: { en: "2. Cleanse your face", vi: "2. Làm sạch da mặt" },
      data: {
        en: [
          "You should usually use the best types of cleansers get rid of excess dirt and oil without completely stripping your skin of moisture.And avoid cleansers that contain soap or scrubbing agents",
        ],
        vi: [
          "Bạn nên  thường xuyên dùng các loại sữa rửa mặt tốt nhất giúp loại bỏ bụi bẩn và dầu thừa mà không làm mất đi độ ẩm hoàn toàn trên da của bạn.Hãy tránh những loại sữa rửa mặt có chứa xà phòng hoặc chất tẩy rửa",
        ],
      },
    },
    {
      title: {
        en: "3. Exfoliate with AHAs or BHAs",
        vi: "3. Tẩy tế bào chết với AHA hoặc BHA",
      },
      data: {
        en: [
          "You should usually follow the American Academy of Dermatology recommends exfoliating just one to two times per week.",
          "If you can, you should always opt for exfoliants with either alpha-hydroxy acids (AHAs) or beta-hydroxy acids (BHAs). ",
        ],
        vi: [
          "Học viện Da liễu Hoa Kỳ khuyến nghị chỉ nên  thường xuyên tẩy tế bào chết 1-2 lần mỗi tuần.",
          "Nếu bạn có thể, hãy  luôn luôn chọn sản phẩm tẩy tế bào chết có axit alpha-hydroxy (AHA) hoặc axit beta-hydroxy (BHA).",
        ],
      },
    },
    {
      title: {
        en: "4. You should  usually Moisturize for balanced hydration",
        vi: "4. Dưỡng ẩm để cân bằng độ ẩm",
      },
      data: {
        en: [],
        vi: [],
      },
    },
    {
      title: {
        en: "5. You should usually use a clay mask",
        vi: "5. Bạn nên thường xuyên sử dụng mặt nạ đất sét",
      },
      data: {
        en: [],
        vi: [],
      },
    },
    {
      title: {
        en: "6. You should usually use Wear sunscreen every day",
        vi: "Thường xuyên thoa kem chống nắng mỗi ngày",
      },
      data: {
        en: [
          "Use a product with an SPF of at least 30. You should apply it at least 15 minutes before you head outside.",
        ],
        vi: [
          "Sử dụng sản phẩm có chỉ số SPF ít nhất là 30. Bạn nên thoa sản phẩm ít nhất 15 phút trước khi ra ngoài. ",
        ],
      },
    },
    {
      title: {
        en: "7. You should always don’t sleep with makeup on",
        vi: "7. Bạn không nên ngủ khi trang điểm",
      },
      data: {
        en: [],
        vi: [],
      },
    },
    {
      title: {
        en: "7. You should always stay hydrated",
        vi: "8. Bạn nên luôn luôn giữ đủ nước",
      },
      data: {
        en: [],
        vi: [],
      },
    },
  ],

  // severe
  [
    {
      title: {
        en: "1. Assess your skin care products",
        vi: "1. Đánh giá các sản phẩm chăm sóc da của bạn",
      },
      data: {
        en: [
          "You should never use Any products designed to clear excess sebum and acne.These products rely on active ingredients such as salicylic acid to remove the top layers of your skin.",
          "To avoid this, you should only always use the following products for a couple of weeks at a time: Astringents; deep-cleaning facial scrubs; oil-based masks.Also, make sure that all your products are noncomedogenic. ",
        ],
        vi: [
          "Bạn nên không  dùng bất kỳ sản phẩm nào được thiết kế để làm sạch bã nhờn dư thừa và mụn trứng cá dựa vào các thành phần hoạt tính như axit salicylic để loại bỏ các lớp trên cùng của da.",
          "Để tránh điều này, chỉ sử dụng  thường xuyên các sản phẩm sau trong vài tuần tại một thời điểm: Chất làm se da; tẩy tế bào chết làm sạch sâu da mặt; mặt nạ dầu. Ngoài ra, hãy đảm bảo rằng tất cả các sản phẩm của bạn đều không gây dị ứng.",
        ],
      },
    },
    {
      title: { en: "2. Cleanse your face", vi: "2. Làm sạch da mặt" },
      data: {
        en: [
          "You should always use the best types of cleansers get rid of excess dirt and oil without completely stripping your skin of moisture.And avoid cleansers that contain soap or scrubbing agents",
        ],
        vi: [
          "Bạn nên luôn luôn dùng các loại sữa rửa mặt tốt nhất giúp loại bỏ bụi bẩn và dầu thừa mà không làm mất đi độ ẩm hoàn toàn trên da của bạn.Hãy tránh những loại sữa rửa mặt có chứa xà phòng hoặc chất tẩy rửa",
        ],
      },
    },
    {
      title: {
        en: "3. Exfoliate with AHAs or BHAs",
        vi: "3. Tẩy tế bào chết với AHA hoặc BHA",
      },
      data: {
        en: [
          "You should always follow the American Academy of Dermatology recommends exfoliating just one to two times per week.",
          "If you can, you should always opt for exfoliants with either alpha-hydroxy acids (AHAs) or beta-hydroxy acids (BHAs). ",
        ],
        vi: [
          "Học viện Da liễu Hoa Kỳ khuyến nghị chỉ nên tẩy tế bào chết 1-2 lần mỗi tuần.",
          "Nếu bạn có thể, hãy  luôn luôn chọn sản phẩm tẩy tế bào chết có axit alpha-hydroxy (AHA) hoặc axit beta-hydroxy (BHA).",
        ],
      },
    },
    {
      title: {
        en: "4. You should always Moisturize for balanced hydration",
        vi: "4. Dưỡng ẩm để cân bằng độ ẩm",
      },
      data: {
        en: [],
        vi: [],
      },
    },
    {
      title: {
        en: "5. You should always use a clay mask",
        vi: "5. Bạn nên thường xuyên sử dụng mặt nạ đất sét",
      },
      data: {
        en: [],
        vi: [],
      },
    },
    {
      title: {
        en: "6. You should always use Wear sunscreen every day",
        vi: "Luôn luôn thoa kem chống nắng mỗi ngày",
      },
      data: {
        en: [
          "Use a product with an SPF of at least 30. You should apply it at least 15 minutes before you head outside.",
        ],
        vi: [
          "Sử dụng sản phẩm có chỉ số SPF ít nhất là 30. Bạn nên thoa sản phẩm ít nhất 15 phút trước khi ra ngoài. ",
        ],
      },
    },
    {
      title: {
        en: "7. You should always don’t sleep with makeup on",
        vi: "7. Bạn không nên ngủ khi trang điểm",
      },
      data: {
        en: [],
        vi: [],
      },
    },
    {
      title: {
        en: "7. You should always stay hydrated",
        vi: "8. Bạn nên luôn luôn giữ đủ nước",
      },
      data: {
        en: [],
        vi: [],
      },
    },
  ],
];

function processPoreResult(data) {
  let sum = 0;
  let result = [];
  for (let i = 0; i < data.length; i++) {
    sum = sum + parseInt(data[i].value);
  }
  if (sum === 0) {
    return null;
  } else {
    if (sum === 1) {
      result.push(levelk8[0]);
      result.push(generalInfoK8[0]);
      result.push(causeArrK8[0]);
    } else if (sum <= 2) {
      result.push(levelk8[1]);
      result.push(generalInfoK8[0]);
      result.push(causeArrK8[1]);
    } else {
      result.push(levelk8[2]);
      result.push(generalInfoK8[0]);
      result.push(causeArrK8[2]);
    }
    return result;
  }
}

// k9
const levelk9 = [
  {
    title: {
      en: "Level of skin spot && acne scar",
      vi: "Mức độ đốm thâm nám và mụn viêm đỏ",
    },
    data: {
      en: ["Light"],
      vi: ["Nhẹ"],
    },
  },
  {
    title: {
      en: "Level of skin spot && acne scar",
      vi: "Mức độ đốm thâm nám và mụn viêm đỏ",
    },
    data: {
      en: ["Medium"],
      vi: ["Trung bình"],
    },
  },
  {
    title: {
      en: "Level of skin spot && acne scar",
      vi: "Mức độ đốm thâm nám và mụn viêm đỏ",
    },
    data: {
      en: ["Severe"],
      vi: ["Nặng"],
    },
  },
];
const resultK9 = [
  // {
  //   title: { en: "Notes for use", vi: "Ghi chú Sử dụng:" },
  //   data: {
  //     en: [
  //       "Only for Women",
  //       "The above parameters are Signal & only part of the assessment of KinSpots/Melasma  and for reference only",
  //       "Currently, the device would show theKinSpots/ Melasma Area ( Updated Ver.)",
  //       "The device would show parameters related to the cause  ( Updated Ver.)",
  //     ],
  //     vi: [
  //       "Chỉ phù hợp với Phụ nữ",
  //       "Các thông số trên chỉ là Dấu hiệu & một phần để đánh giá tình trạng Đốm thâm/nám & chỉ để tham khảo",
  //       "Thiết bị sẽ thể hiện vùng bị Nám da ( trong bảng cập nhật)",
  //       "Thiết bị sẽ thể hiện thông số liên quan đến nguyên nhân gây Thâm nám da ( trong bảng cập nhật)",
  //     ],
  //   },
  // },
  {
    title: {
      en: "Coping and living with melasma (healthline.com)",
      vi: "Phòng ngừa bệnh Nám da (vinmec.com)",
    },
    data: {
      en: [
        "While not all cases of melasma will clear up with treatment, there are things you can do to make sure the condition doesn’t get worse and to minimize the appearance of the discoloration. These include:",
        "·         using makeup to cover areas of discoloration",
        "·         taking prescribed medication",
        "·         wearing sunscreen every day with SPF 30",
        "·         wearing a wide-brimmed hat that shields or provides shade for your face",
        "·         Wearing protective clothing is especially important if you’ll be in the sun for an extended period of time.",
      ],
      vi: [
        "Việc điều trị nám da thường không đơn giản và khá tốn kém. Tuy nhiên, các biện pháp sau có thể giúp phòng ngừa các vấn đề tăng sắc tố da nói chung và nám da nói riêng.",
        "·         Tránh tiếp xúc với ánh nắng mặt trời từ 10h đến 15h:",
        "·         Bôi kem chống nắng:",
        "·         Che chắn làn da bằng các trang phục chống nắng:",
        "·         Chế độ dinh dưỡng:",
        "·         Sử dụng mỹ phẩm được kiểm định an toàn:",
        "·         Đi khám bác sĩ da liễu khi có các vấn đề trên da:",
      ],
    },
  },
];
function processSkinSpotResult(data) {
  let sum = 0;
  let result = [];
  for (let i = 0; i < data.length; i++) {
    sum = sum + parseInt(data[i].value);
  }
  // if (sum === 0) {
  //   return null;
  // } else {
  //   if (sum < 20) {
  //     result.push(levelk9[0]);
  //   } else if (sum <= 50) {
  //     result.push(levelk9[1]);
  //   } else {
  //     result.push(levelk9[2]);
  //   }
  //   result.push(resultK9[0]);
  //   result.push(resultK9[1]);
  //   return result;
  // }
  return null;
}

module.exports = {
  processAginSkinResult,
  processAcneResult,
  processDarkCircleResult,
  processPoreResult,
  processSkinSpotResult,
};
