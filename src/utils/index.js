const Datauri = require("datauri");
const path = require("path");

const cloudinary = require("../config/cloudinary");
const sgMail = require("@sendgrid/mail");
var crypto = require("crypto");
var base64 = require('base-64');
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

function uploader(req) {
  return new Promise((resolve, reject) => {
    const dUri = new Datauri();
    let image = dUri.format(
      path.extname(req.file.originalname).toString(),
      req.file.buffer
    );

    cloudinary.uploader.upload(image.content, (err, url) => {
      if (err) return reject(err);
      return resolve(url);
    });
  });
}

function sendEmail(mailOptions) {
  return new Promise((resolve, reject) => {
    sgMail.send(mailOptions, (error, result) => {
      if (error) return reject(error);
      return resolve(result);
    });
  });
}

function generateToken(data) {
  // Create the generator:
  var randtoken = require("rand-token").generator({
    source: crypto.randomBytes,
  });
  return base64.encode(data + "_" + randtoken.generate(16));
}

function getTypeSdk(data) {
  try {
    let dataInput = base64.decode(data)
    if (dataInput !== '') {
      return dataInput.split('_')[1] !== 'undefined' ? dataInput.split('_')[1]  : null
    }  else {
      return null
    }
  }catch(e) {
    return null;
  }
  
}

function configLimitCall(type ='trial') {
  switch(type) {
    case 'trial':
      return 200
    case 'month':
      return 4000
    case 'year':
      return 6000
    default: return 1;
  }
}
function configDayCall(type ='trial') {
  switch(type) {
    case 'trial':
      return 2
    case 'month':
      return 30
    case 'year':
      return 360
    default: return 1;
  }
}

module.exports = { uploader, sendEmail, generateToken, configLimitCall, configDayCall , getTypeSdk };
