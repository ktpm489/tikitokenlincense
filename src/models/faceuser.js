const mongoose = require("mongoose");

const faceUserSchema = new mongoose.Schema(
  {
    user_id: {
      type: mongoose.Schema.Types.ObjectId,
      index: true,
    },
    facedatares: {
      type: mongoose.Schema.Types.Mixed,
      required: true,
      // index: true,
    },
    imageInfo: {
      type: mongoose.Schema.Types.Mixed,
    },
    generalResult: {
      type: mongoose.Schema.Types.Mixed,
      required: true,
      // index: true,
    },
    specialResult: {
      type: mongoose.Schema.Types.Mixed,
      required: true,
      // index: true,
    },
    generalConclusion: {
      type: mongoose.Schema.Types.Mixed,
      required: true,
      // index: true,
    },
    specialConclusion: {
      type: mongoose.Schema.Types.Mixed,
      required: true,
      // index: true,
    },
    hintResult: {
      type: mongoose.Schema.Types.Mixed,
      required: true,
      // index: true,
    },
    createdAt: {
      type: Date,
      required: true,
      default: Date.now,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("SdkFaceUser", faceUserSchema);
