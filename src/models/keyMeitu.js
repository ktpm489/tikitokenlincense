const mongoose = require("mongoose");

const tokenSchema = new mongoose.Schema(
  {
    key: {
      type: String,
      required: true,
    }
  }
);

tokenSchema.method("toJSON", function () {
  const { __v, _id, ...object } = this.toObject();
  object.id = _id;
  return object;
});

module.exports = mongoose.model("keymeitu", tokenSchema);
