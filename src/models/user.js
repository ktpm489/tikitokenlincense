const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const crypto = require('crypto');
const {generateToken, configLimitCall, configDayCall} = require('../utils/index');
const Token = require('../models/token');
const Tracking = require('../models/tracking');


const UserSchema = new mongoose.Schema({
    email: {
        type: String,
        unique: true,
        index: true,
        required: 'Your email is required',
        trim: true
    },
    username: {
        type: String,
        unique: true,
        required: false,
        index: true,
        sparse: true
    },
    password: {
        type: String,
        required: 'Your password is required',
        max: 100,
    },

    firstName: {
        type: String,
        required: 'First Name is required',
        max: 100
    },

    lastName: {
        type: String,
        required: 'Last Name is required',
        max: 100
    },
    type: {
        type: String,
        default:'trial',
    },
    role: {
        type: String,
        default :'user'
    },
    bio: {
        type: String,
        required: false,
        max: 255
    },
    profileImage: {
        type: String,
        required: false,
        max: 255
    },
    isVerified: {
        type: Boolean,
        default: false
    },
    isPay: {
        type: Boolean,
        default: false
    },
    resetPasswordToken: {
        type: String,
        required: false
    },
    resetPasswordExpires: {
        type: Date,
        required: false
    }
}, {timestamps: true});


UserSchema.pre('save',  function(next) {
    const user = this;

    if (!user.isModified('password')) return next();

    bcrypt.genSalt(10, function(err, salt) {
        if (err) return next(err);
        bcrypt.hash(user.password, salt, function(err, hash) {
            if (err) return next(err);
            user.password = hash;
            next();
        });
    });
});

UserSchema.methods.comparePassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

UserSchema.methods.generateJWT = function(tokenId) {
    const today = new Date();
    const expirationDate = new Date(today);
    expirationDate.setDate(today.getDate() + 60000);

    let payload = {
        id: this._id,
        tokenId: tokenId,
        email: this.email,
        username: this.username,
        firstName: this.firstName,
        lastName: this.lastName,
        role: this.role
    };

    return jwt.sign(payload, process.env.JWT_SECRET, {
        expiresIn: parseInt(expirationDate.getTime() / 1000, 10)
    });
};

UserSchema.methods.generatePasswordReset = function() {
    this.resetPasswordToken = crypto.randomBytes(20).toString('hex');
    this.resetPasswordExpires = Date.now() + 3600000; //expires in an hour
};

UserSchema.methods.generateVerificationToken = function() {
    let payload = {
        userId: this._id,
        token: crypto.randomBytes(20).toString('hex')
    };
    return new Token(payload);
};

UserSchema.methods.generateTrackingToken = function(typeSdkInput) {
    let generateTokenData = generateToken(this._id + '_' + typeSdkInput)
    let type = this.type ? this.type : 'trial'
    let typeSdk = typeSdkInput ? typeSdkInput : '1'
    let payload = {
        userId: this._id,
        typeSdk: typeSdk,
        token: generateTokenData,
        limit: configLimitCall(type),
        dateExpires: Date.now() + configDayCall(type)*1000*60*60*24
    };
    // console.log('Tracking',payload)
    return new Tracking(payload);
};

module.exports = mongoose.model('SdkUsers', UserSchema);