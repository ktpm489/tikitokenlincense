const mongoose = require('mongoose');

const trackingSchema = new mongoose.Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        index: true,
        ref: 'User'
    },
    token: {
        type: String,
        index: true,
        required: true
    },
    typeSdk: {
        type: String,
        default : '1',
        required: true
    },
    limit : {
        type : Number,
        required : true
    },
    dateExpires: {
        type: Date,
        required: true,
        default: Date.now
    },
    count : {
        type: Number,
        require : true,
        default: 0,
    },
    isValid: {
        type: Boolean,
        default: true
    },
    createdAt: {
        type: Date,
        required: true,
        default: Date.now
    }
}, {timestamps: true});

module.exports = mongoose.model('SdkTracking', trackingSchema);
