const mongoose = require("mongoose");

const faceUserSchema = new mongoose.Schema(
  {
    email: {
      type: String,
      required: true,
      index: true,
    },
    typesdk: {
      type: String,
      required: true,
      index: true,
      default: "",
    },
    token: {
      type: String,
      index: true,
      required: true,
      default: "",
    },
    facedata: {
      type: mongoose.Schema.Types.Mixed,
      required: true,
      index: true,
    },
    createdAt: {
      type: Date,
      required: true,
      default: Date.now,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("SdkUserSkin", faceUserSchema);
