const fetch = require("node-fetch");
const cheerio = require("cheerio");
const axios = require("axios");
const https = require("https");

// At request level
const agent = new https.Agent({
  rejectUnauthorized: false,
});
// const useragent = require("express-useragent");
const randomUseragent = require("random-useragent");
async function loadConfigAI() {
  try {
    let response = await axios.get("https://justpaste.it/7yhov", {
      httpsAgent: agent,
    });
    const $ = cheerio.load(response.data);
    let data = $("#articleContent").children("p").text();
    if (data !== "") {
      return data;
    } else {
      data = $("#articleContent")
        .children("div")
        .children("div")
        .children("span")
        .text();
      if (data !== "") {
        return data;
      } else {
        return $("#articleContent").children("p").children("span").text();
      }
    }
    console.log("data", data);
    return data;
  } catch (e) {
    console.log("e", e);
    return "";
  }
}

async function callData(data) {
  try {
    let keyApi = await loadConfigAI();
    let random = randomUseragent.getRandom(function (ua) {
      return ua.browserName === "Firefox";
    });
    console.log("random", random);
    let res = await fetch("https://openapi.mtlab.meitu.com/v3/makeup", {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
        "Accept-Encoding": "gzip, deflate, br",
        authority: "openapi.mtlab.meitu.com",
        authorization: keyApi,
        authorizationtype: 1,
        // "user-agent":
        //   "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.192 Safari/537.36",
        "user-agent": random,
      },
    });
    console.log("res", res);
    let json = await res.json();
    console.log("json", json);
    // console.log(json.media_info_list[0].media_extra.faces[0].face_attributes);
  } catch (e) {
    console.log("e", e);
  }
}

callData({
  "parameter": {
      "makeupId": "Fb0020A7UaiQjarZ",
      "makeupAlpha": 100,
      "beautyAlpha": 0,
      "hairMask": 0,
      "rsp_media_type": "url"
  },
  "extra": {},
  "media_info_list": [
      {
          "media_data": "https://obs.mtlab.meitu.com/public/itc2_gumd4GoGYxjW9Ncq5rzVlVzmnnT/demo/makeup/005.jpg",
          "media_profiles": {
              "media_data_type": "url"
          }
      }
  ]
});
